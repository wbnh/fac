package ch.bwe.fac.v1.installer.feature;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class FeatureResult {

	private List<Problem> problems = new LinkedList<Problem>();
	private String featureAction;

	public FeatureResult(String featureAction) {
		this.featureAction = featureAction;
	}

	public boolean isSuccessful() {
		return problems.isEmpty();
	}

	public void addProblem(Problem problem) {
		if (problem != null) {
			problems.add(problem);
		}
	}

	public void addProblems(Collection<Problem> problems) {
		if (problems != null) {
			problems.addAll(problems);
		}
	}

	public void addProblems(FeatureResult result) {
		if (result != null) {
			problems.addAll(result.getProblems());
		}
	}

	public List<Problem> getProblems() {
		return problems;
	}

	public String getFeatureAction() {
		return featureAction;
	}

	public static class Problem {

		private Object executor;
		private String action;
		private String description;
		private Throwable throwable;

		/**
		 * Constructor handling initialization of mandatory fields.
		 *
		 */
		public Problem(Object executor) {
			this.executor = executor;
		}

		public String getDescription() {
			return description;
		}

		public Problem setDescription(String description) {
			this.description = description;
			return this;
		}

		public Throwable getThrowable() {
			return throwable;
		}

		public Problem setThrowable(Throwable throwable) {
			this.throwable = throwable;
			return this;
		}

		public String getAction() {
			return action;
		}

		public Problem setAction(String action) {
			this.action = action;
			return this;
		}

		/**
		 * @return the executor
		 */
		public Object getExecutor() {
			return executor;
		}
	}
}
