package ch.bwe.fac.v1.installer.feature.database;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.Version;
import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.installer.feature.FeatureResult;
import ch.bwe.fac.v1.installer.feature.FeatureResult.Problem;

/**
 * Feature to install data.
 *
 * @author Benjamin Weber
 *
 */
public class AbstractDatabaseDataFeature extends AbstractDatabaseFeature {

	private List<AbstractDatabasePersistable> persistables = new LinkedList<>();

	protected AbstractDatabaseDataFeature(Version version, String action, EntityManager entityManager,
					List<AbstractDatabasePersistable> persistables, Environment... allowedEnvironments) {
		super(version, action, entityManager, allowedEnvironments);
		if (persistables != null)
			this.persistables = persistables;
	}

	protected AbstractDatabaseDataFeature(Version version, String action, EntityManager entityManager,
					List<AbstractDatabasePersistable> persistables, List<Environment> allowedEnvironments) {
		super(version, action, entityManager, allowedEnvironments);
		if (persistables != null)
			this.persistables = persistables;
	}

	protected final void setPersistables(List<AbstractDatabasePersistable> persistables) {
		this.persistables = persistables;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected FeatureResult doPreInstallCheck() {
		FeatureResult result = new FeatureResult(getAction());
		// no check for data
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected FeatureResult doPerformInstall() {
		FeatureResult result = new FeatureResult(getAction());

		try {
			int insertedRows = 0;
			entitymanager.getTransaction().begin();
			for (AbstractDatabasePersistable persistable : persistables) {
				try {
					entitymanager.persist(persistable);
					insertedRows++;
				} catch (EntityExistsException e) {
					ServiceRegistry.getLogProxy().error(this, "Could not insert persistable - already exists", e);
					// continue, we want the other persistables to be inserted
				}
			}
			entitymanager.getTransaction().commit();
			if (persistables.size() != insertedRows) {
				result.addProblem(new Problem(this).setAction("Insert data").setDescription(
								"Expected " + persistables.size() + " rows to be inserted, but there were " + insertedRows));
			}
		} catch (IllegalArgumentException e) {
			ServiceRegistry.getLogProxy().error(this, "Could not insert data", e);
			result.addProblem(
							new Problem(this).setAction("Insert data").setDescription(e.getLocalizedMessage()).setThrowable(e));
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected FeatureResult doPostInstallCheck() {
		FeatureResult result = new FeatureResult(getAction());
		// no check for data
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected FeatureResult doPerformUninstall() {
		FeatureResult result = new FeatureResult(getAction());

		try {
			for (AbstractDatabasePersistable persistable : persistables) {
				entitymanager.remove(persistable);
			}
		} catch (IllegalArgumentException e) {
			ServiceRegistry.getLogProxy().error(this, "Could not delete data", e);
			result.addProblem(
							new Problem(this).setAction("Delete data").setDescription(e.getLocalizedMessage()).setThrowable(e));
		}

		return result;
	}

}
