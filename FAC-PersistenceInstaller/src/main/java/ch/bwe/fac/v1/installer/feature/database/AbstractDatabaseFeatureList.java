package ch.bwe.fac.v1.installer.feature.database;

import java.sql.Connection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.util.Version;
import ch.bwe.fac.v1.installer.feature.FeatureResult;

public abstract class AbstractDatabaseFeatureList extends AbstractDatabaseFeature {

	private List<AbstractDatabaseFeature> features = new LinkedList<AbstractDatabaseFeature>();

	protected AbstractDatabaseFeatureList(Version version, String action, Connection connection,
					Environment... allowedEnvironments) {
		super(version, action, connection, allowedEnvironments);
	}

	public AbstractDatabaseFeatureList(Version version, String action, Connection connection,
					List<Environment> allowedEnvironments) {
		super(version, action, connection, allowedEnvironments);
	}

	protected AbstractDatabaseFeatureList(Version version, String action, EntityManager entityManager,
					Environment... allowedEnvironments) {
		super(version, action, entityManager, allowedEnvironments);
	}

	public AbstractDatabaseFeatureList(Version version, String action, EntityManager entityManager,
					List<Environment> allowedEnvironments) {
		super(version, action, entityManager, allowedEnvironments);
	}

	public void addFeature(AbstractDatabaseFeature feature) {
		features.add(feature);
	}

	@Override
	protected FeatureResult doPreInstallCheck() {
		FeatureResult result = new FeatureResult(getAction());

		for (AbstractDatabaseFeature feature : features) {
			FeatureResult preInstallResult = feature.preInstallCheck();
			if (!preInstallResult.isSuccessful()) {
				result.addProblems(preInstallResult);
			}
		}

		return result;
	}

	@Override
	protected FeatureResult doPerformInstall() {
		FeatureResult result = new FeatureResult(getAction());

		for (AbstractDatabaseFeature feature : features) {
			FeatureResult installResult = feature.performInstall();
			if (!installResult.isSuccessful()) {
				result.addProblems(installResult);
			}
		}

		return result;
	}

	@Override
	protected FeatureResult doPostInstallCheck() {
		FeatureResult result = new FeatureResult(getAction());

		for (AbstractDatabaseFeature feature : features) {
			FeatureResult postInstallResult = feature.postInstallCheck();
			if (!postInstallResult.isSuccessful()) {
				result.addProblems(postInstallResult);
			}
		}

		return result;
	}

	@Override
	protected FeatureResult doPerformUninstall() {
		FeatureResult result = new FeatureResult(getAction());

		List<AbstractDatabaseFeature> features = new LinkedList<>(this.features);
		Collections.reverse(features);

		for (AbstractDatabaseFeature feature : features) {
			FeatureResult uninstallResult = feature.performUninstall();
			if (!uninstallResult.isSuccessful()) {
				result.addProblems(uninstallResult);
			}
		}
		return result;
	}

}
