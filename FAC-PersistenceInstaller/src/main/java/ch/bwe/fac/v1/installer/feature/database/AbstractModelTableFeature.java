package ch.bwe.fac.v1.installer.feature.database;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.util.List;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.Version;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.HistoryIdentifier;
import ch.bwe.fac.v1.type.Identifier;

/**
 * Feature to install models and updated models.
 *
 * @author Benjamin Weber
 */
public abstract class AbstractModelTableFeature extends AbstractTableFeature {

  /**
   * Constructor.
   * 
   * @param version             the application version for which the feature was
   *                            introduced
   * @param action              the action we are performing
   * @param persistable         the entity to create
   * @param connection          the DB connection to use
   * @param allowedEnvironments the environments in which we can install the
   *                            feature
   */
  protected AbstractModelTableFeature(Version version, String action, Class<? extends DatabaseModel> persistable,
      Connection connection, Environment... allowedEnvironments) {
    super(version, action, connection, allowedEnvironments);
    init(persistable);
  }

  /**
   * Constructor.
   * 
   * @param version             the application version for which the feature was
   *                            introduced
   * @param action              the action we are performing
   * @param persistable         the entity to create
   * @param connection          the DB connection to use
   * @param allowedEnvironments the environments in which we can install the
   *                            feature
   */
  protected AbstractModelTableFeature(Version version, String action, Class<? extends DatabaseModel> persistable,
      Connection connection, List<Environment> allowedEnvironments) {
    super(version, action, connection, allowedEnvironments);
    init(persistable);
  }

  /**
   * Initialise
   */
  private void init(Class<? extends DatabaseModel> persistable) {
    createHistory();
    name(persistable.getSimpleName());

    addColumns(persistable);
  }

  private void addColumns(Class<? extends DatabaseModel> persistable) {
    // ID etc.
    Field[] declaredFields = persistable.getDeclaredFields();
    addFields(declaredFields);

    try {

      for (Field field : declaredFields) {
        if ("auditFields".equals(field.getName())) {
          addFields(field.getType().getDeclaredFields());
        } else if ("content".equals(field.getName())) {
          addFields(field.getType().getDeclaredFields());
        } else if ("references".equals(field.getName())) {
          addFields(field.getType().getDeclaredFields());
        }
      }

    } catch (SecurityException | IllegalArgumentException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not add fields", e);
    }

    addSuperclassPrimaryKey(persistable);
  }

  private void addFields(Field[] fields) {
    for (Field field : fields) {
      field.setAccessible(true);
      if (field.isAnnotationPresent(Attribute.class)) {
        ModelColumnFeature column = new ModelColumnFeature(getVersion(), getAction(), getCurrentConnection(),
            getAllowedEnvironments());
        column(column);

        column.name(field.getName());
        if (field.isAnnotationPresent(Identifier.class)) {
          column.nonNull();
          column.primaryKey();
        } else if (field.isAnnotationPresent(HistoryIdentifier.class)) {
          column.nonNull();
          column.historyPrimaryKey();
          column.defaultValue("0");
        }
        if (DatabaseModel.class.isAssignableFrom(field.getType())) {
          column.references(field.getType().getSimpleName(), "recordId");
          column.type(Integer.class);
          column.name(field.getName() + "RecordId");

          ModelColumnFeature historyColumn = new ModelColumnFeature(getVersion(), getAction(), getCurrentConnection(),
              getAllowedEnvironments());
          historyColumn.name(field.getName() + "RevisionId");
          historyColumn.references(field.getType().getSimpleName() + HISTORY_POSTFIX, "revisionId");
          historyColumn.type(Integer.class);
          historyOnlyColumn(historyColumn);
        } else {
          column.type(field.getType());
          column.size(field.getAnnotation(Attribute.class).length());
        }
      }
    }
  }

  @SuppressWarnings("unchecked")
  private void addSuperclassPrimaryKey(Class<? extends DatabaseModel> persistable) {
    Class<?> superclass = persistable.getSuperclass();

    if (DatabaseModel.class.equals(superclass)) { return; }

    if (DatabaseModel.class.isAssignableFrom(superclass)) {
      Field[] fields = superclass.getDeclaredFields();
      for (Field field : fields) {
        if (field.isAnnotationPresent(Identifier.class)) {
          ModelColumnFeature column = new ModelColumnFeature(getVersion(), getAction(), getCurrentConnection(),
              getAllowedEnvironments());
          column(column);
          column.name(field.getName());
          column.nonNull();
          column.primaryKey();

          column.type(field.getType());
          column.references(superclass.getSimpleName(), field.getName());
        }
      }

      addSuperclassPrimaryKey((Class<? extends DatabaseModel>) superclass);
    }
  }
}
