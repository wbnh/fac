package ch.bwe.fac.v1.installer.feature.database;

public interface ColumnTypes {

	String String(int size);

	String Integer();

	String Boolean();

	String LocalDateTime();

	String LocalDate();
}
