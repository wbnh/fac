package ch.bwe.fac.v1.installer;

import java.util.HashMap;
import java.util.Map;

public class InstallerRegistry {

	public static final String CONNECTION_URL = "connectionUrl";
	public static final String CONNECTION_USER = "connectionUser";
	public static final String CONNECTION_PASSWORD = "connectionPassword";

	private static InstallerRegistry instance = new InstallerRegistry();

	public static InstallerRegistry getInstance() {
		return instance;
	}

	private InstallerRegistry() {
	}

	private Map<String, Object> values = new HashMap<>();

	public void put(String key, Object value) {
		values.put(key, value);
	}

	public Object get(String key) {
		return values.get(key);
	}
}
