package ch.bwe.fac.v1.installer.feature.database;

import java.sql.Connection;
import java.util.List;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.util.Version;

/**
 * Feature for columns of model fields.
 *
 * @author Benjamin Weber
 *
 */
class ModelColumnFeature extends AbstractColumnFeature {

	ModelColumnFeature(Version version, String action, Connection connection, List<Environment> allowedEnvironments) {
		super(version, action, connection, allowedEnvironments);
	}
}
