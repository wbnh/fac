package ch.bwe.fac.v1.installer.feature.database;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.Version;
import ch.bwe.fac.v1.installer.feature.FeatureResult;
import ch.bwe.fac.v1.installer.feature.FeatureResult.Problem;

public abstract class AbstractScriptFeature extends AbstractDatabaseFeature {

	private Path preCheckScript;
	private Path installScript;
	private Path postCheckScript;
	private Path uninstallScript;

	protected AbstractScriptFeature(Version version, String action, Connection connection,
					Environment... allowedEnvironments) {
		super(version, action, connection, allowedEnvironments);
	}

	protected AbstractScriptFeature(Version version, String action, Connection connection,
					List<Environment> allowedEnvironments) {
		super(version, action, connection, allowedEnvironments);
	}

	public AbstractScriptFeature preCheckScript(Path script) {
		preCheckScript = script;
		return this;
	}

	public AbstractScriptFeature installScript(Path script) {
		installScript = script;
		return this;
	}

	public AbstractScriptFeature postCheckScript(Path script) {
		postCheckScript = script;
		return this;
	}

	public AbstractScriptFeature uninstallScript(Path script) {
		uninstallScript = script;
		return this;
	}

	protected String loadScript(Path script) throws IOException {
		List<String> lines = Files.readAllLines(script);
		StringBuilder builder = new StringBuilder();
		for (String line : lines) {
			builder.append(line);
			builder.append('\n');
		}
		return builder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected FeatureResult doPreInstallCheck() {
		FeatureResult result = new FeatureResult(getAction());

		try {
			executeStatement(loadScript(preCheckScript));
		} catch (SQLException | IOException e) {
			ServiceRegistry.getLogProxy().error(this, "Could not perform pre-install check", e);
			result.addProblem(new Problem(this).setAction("Executing pre-install script")
							.setDescription(e.getLocalizedMessage()).setThrowable(e));
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected FeatureResult doPerformInstall() {
		FeatureResult result = new FeatureResult(getAction());

		try {
			executeStatement(loadScript(installScript));
		} catch (SQLException | IOException e) {
			ServiceRegistry.getLogProxy().error(this, "Could not perform install", e);
			result.addProblem(new Problem(this).setAction("Executing install script").setDescription(e.getLocalizedMessage())
							.setThrowable(e));
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected FeatureResult doPostInstallCheck() {
		FeatureResult result = new FeatureResult(getAction());

		try {
			executeStatement(loadScript(postCheckScript));
		} catch (SQLException | IOException e) {
			ServiceRegistry.getLogProxy().error(this, "Could not perform post-install check", e);
			result.addProblem(new Problem(this).setAction("Executing post-install script")
							.setDescription(e.getLocalizedMessage()).setThrowable(e));
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected FeatureResult doPerformUninstall() {
		FeatureResult result = new FeatureResult(getAction());

		try {
			executeStatement(loadScript(uninstallScript));
		} catch (SQLException | IOException e) {
			ServiceRegistry.getLogProxy().error(this, "Could not perform uninstall", e);
			result.addProblem(new Problem(this).setAction("Executing uninstall script")
							.setDescription(e.getLocalizedMessage()).setThrowable(e));
		}

		return result;
	}
}
