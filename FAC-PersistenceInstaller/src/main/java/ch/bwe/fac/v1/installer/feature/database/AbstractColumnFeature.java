package ch.bwe.fac.v1.installer.feature.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.List;
import java.util.UUID;

import org.hibernate.mapping.Collection;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.Version;
import ch.bwe.fac.v1.installer.feature.FeatureResult;
import ch.bwe.fac.v1.installer.feature.FeatureResult.Problem;

public abstract class AbstractColumnFeature extends AbstractDatabaseFeature {

  private static final String CREATE_KEY = "sql.column.create";
  private static final String CREATE_DEFAULT_KEY = "sql.column.create.default";
  private static final String ALTER_TABLE_KEY = "sql.table.alter.add";
  private static final String EXISTS_KEY = "sql.column.exists";
  private static final String DROP_KEY = "sql.column.drop";
  private static final String FOREIGN_KEY_CONSTRAINT_KEY = "sql.table.foreignkey";

  private Configuration statements;
  private Configuration types;

  private String name;
  private Class<?> type;
  private int size;
  private boolean nonNull;
  private boolean primaryKey;
  private boolean historyPrimaryKey;
  private String refTableName;
  private String refColumnName;
  private String tableName;
  private String refTablePostfix = "";
  private String defaultValue;

  protected AbstractColumnFeature(Version version, String action, Connection connection,
      Environment... allowedEnvironments) {
    super(version, action, connection, allowedEnvironments);
  }

  protected AbstractColumnFeature(Version version, String action, Connection connection,
      List<Environment> allowedEnvironments) {
    super(version, action, connection, allowedEnvironments);
  }

  public AbstractColumnFeature name(String name) {
    this.name = name;
    return this;
  }

  public AbstractColumnFeature type(Class<?> type) {
    if (Collection.class.isAssignableFrom(type)) { throw new IllegalArgumentException("Type cannot be a collection"); }
    this.type = type;
    return this;
  }

  public AbstractColumnFeature size(int size) {
    this.size = size;
    return this;
  }

  public AbstractColumnFeature nonNull() {
    nonNull = true;
    return this;
  }

  public AbstractColumnFeature primaryKey() {
    primaryKey = true;
    historyPrimaryKey = false;
    return this;
  }

  public AbstractColumnFeature historyPrimaryKey() {
    historyPrimaryKey = true;
    primaryKey = false;
    return this;
  }

  public AbstractColumnFeature references(String table, String column) {
    refTableName = table;
    refColumnName = column;
    return this;
  }

  public AbstractColumnFeature tableName(String tableName) {
    this.tableName = tableName;
    return this;
  }

  public AbstractColumnFeature referenceTablePostfix(String postfix) {
    refTablePostfix = postfix;
    return this;
  }

  public AbstractColumnFeature defaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
    return this;
  }

  protected boolean executeStatement(String statement, boolean history) throws SQLException {
    return executeStatement(statement, history, true);
  }

  protected boolean executeStatement(String statement, boolean history, boolean allowAutoIncrement)
      throws SQLException {
    statement = formatStatement(statement, history, allowAutoIncrement);

    return super.executeStatement(statement);
  }

  @Override
  protected boolean executeStatement(String statement) throws SQLException {
    statement = formatStatement(statement, false, true);

    return super.executeStatement(statement);
  }

  private String formatStatement(String statement, boolean history, boolean allowAutoIncrement) throws SQLException {
    // {0}: Schema Name
    // {1}: Column Name
    // {2}: Column Type
    // {3}: Not Null
    // {4}: UUID
    // {5}: Ref Table Name
    // {6}: Ref Column Name
    // {7}: Table Name

    String notNull = (nonNull ? "NOT NULL" : "")
        + (primaryKey && !history && allowAutoIncrement ? " AUTO_INCREMENT " : "");

    statement = MessageFormat.format(statement, getSchema(), name, getColumnType(), notNull, createRandomUuid(),
        refTableName + refTablePostfix, refColumnName, tableName, defaultValue);
    return statement;
  }

  private String createRandomUuid() {
    String uuid = UUID.randomUUID().toString();
    return uuid.replaceAll("\\-", "");
  }

  @Override
  protected FeatureResult doPreInstallCheck() {
    loadStatements();
    final String action = "Check table existence";
    FeatureResult result = new FeatureResult(getAction());

    try {
      boolean exists = executeStatement(statements.getStringValue(EXISTS_KEY));

      if (exists) {
        result.addProblem(new Problem(this).setAction(action).setDescription("Column exists"));
      }
    } catch (SQLException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not check column existence", e);
      result.addProblem(new Problem(this).setAction(action).setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  @Override
  protected FeatureResult doPerformInstall() {
    loadStatements();
    FeatureResult result = new FeatureResult(getAction());

    String key = ALTER_TABLE_KEY;

    try {
      executeStatement(statements.getStringValue(key));
    } catch (SQLException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not create column", e);
      result.addProblem(
          new Problem(this).setAction("Create Column").setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  String getColumnDefinition(boolean history, boolean allowAutoIncrement) {
    loadStatements();

    boolean resetDefaultValue = false;
    if (defaultValue == null) {
      resetDefaultValue = true;
      if (history && (primaryKey || historyPrimaryKey)) {
        defaultValue = "0";
      }
    }

    String key = defaultValue == null ? CREATE_KEY : CREATE_DEFAULT_KEY;

    String statement = statements.getStringValue(key);

    try {
      statement = formatStatement(statement, history, allowAutoIncrement);
    } catch (SQLException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not get schema", e);
      throw new RuntimeException(e);
    } finally {
      if (resetDefaultValue) {
        defaultValue = null;
      }
    }
    return statement;
  }

  String getForeignKeyDefinition(boolean history, boolean allowAutoIncrement) {
    loadStatements();

    String key = FOREIGN_KEY_CONSTRAINT_KEY;

    String statement = statements.getStringValue(key);

    try {
      statement = formatStatement(statement, history, allowAutoIncrement);
    } catch (SQLException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not get schema", e);
      throw new RuntimeException(e);
    }
    return statement;
  }

  public boolean isForeignKey() {
    return refColumnName != null && refTableName != null;
  }

  @Override
  protected FeatureResult doPostInstallCheck() {
    loadStatements();
    final String action = "Check table existence";
    FeatureResult result = new FeatureResult(getAction());

    try {
      boolean exists = executeStatement(statements.getStringValue(EXISTS_KEY));

      if (!exists) {
        result.addProblem(new Problem(this).setAction(action).setDescription("Column does not exist"));
      }
    } catch (SQLException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not check column existence", e);
      result.addProblem(new Problem(this).setAction(action).setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  @Override
  protected FeatureResult doPerformUninstall() {
    loadStatements();
    FeatureResult result = new FeatureResult(getAction());

    try {
      executeStatement(statements.getStringValue(DROP_KEY));
    } catch (SQLException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not drop column", e);
      result.addProblem(
          new Problem(this).setAction("Drop Column").setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  private String getColumnType() {
    if (types == null) {
      types = ServiceRegistry.getConfigurationProxy()
          .getConfiguration(new ConfigurationProperties<>(ColumnTypes.class).setVariantId(getDbType().toString()));
    }
    return MessageFormat.format(types.getStringValue(type.getSimpleName()), Integer.toString(size));
  }

  public boolean isPrimaryKey() {
    return primaryKey;
  }

  public boolean isHistoryPrimaryKey() {
    return historyPrimaryKey || primaryKey;
  }

  public String getName() {
    return name;
  }

  private void loadStatements() {
    if (statements == null) {
      statements = ServiceRegistry.getConfigurationProxy().getConfiguration(
          new ConfigurationProperties<>(AbstractColumnFeature.class).setVariantId(getDbType().toString()));
    }
  }
}
