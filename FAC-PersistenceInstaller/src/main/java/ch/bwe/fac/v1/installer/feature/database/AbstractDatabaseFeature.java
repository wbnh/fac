package ch.bwe.fac.v1.installer.feature.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.Version;
import ch.bwe.fac.v1.installer.InstallerRegistry;
import ch.bwe.fac.v1.installer.feature.AbstractFeature;
import ch.bwe.fac.v1.installer.feature.FeatureResult;
import ch.bwe.fac.v1.installer.feature.FeatureResult.Problem;

public abstract class AbstractDatabaseFeature extends AbstractFeature {

	private static final String TABLE_COLUMNS_KEY = "sql.table.columns";

	private Connection outsideConnection;
	private Connection currentConnection;
	private DbmsType dbType;

	protected EntityManager entitymanager;

	private Configuration statements;

	protected AbstractDatabaseFeature(Version version, String action, Connection connection,
					Environment... allowedEnvironments) {
		super(version, action, allowedEnvironments);
		outsideConnection = connection;
		currentConnection = outsideConnection;
		initDbmsType();
	}

	protected AbstractDatabaseFeature(Version version, String action, Connection connection,
					List<Environment> allowedEnvironments) {
		super(version, action, allowedEnvironments);
		outsideConnection = connection;
		currentConnection = outsideConnection;
		initDbmsType();
	}

	protected AbstractDatabaseFeature(Version version, String action, EntityManager entitymanager,
					Environment... allowedEnvironments) {
		super(version, action, allowedEnvironments);
		this.entitymanager = entitymanager;
	}

	protected AbstractDatabaseFeature(Version version, String action, EntityManager entitymanager,
					List<Environment> allowedEnvironments) {
		super(version, action, allowedEnvironments);
		this.entitymanager = entitymanager;
	}

	private void initDbmsType() {
		if (currentConnection == null) {
			dbType = null;
			statements = null;
			return;
		}

		for (DbmsType type : DbmsType.values()) {
			try {
				if (currentConnection.getMetaData().getDatabaseProductName().toLowerCase()
								.contains(type.toString().toLowerCase())) {
					dbType = type;
					initStatements();
					return;
				}
			} catch (SQLException e) {
				ServiceRegistry.getLogProxy().error(this, "Could not check DBMS type", e);
				throw new RuntimeException(e);
			}
		}
	}

	protected void initStatements() {
		statements = ServiceRegistry.getConfigurationProxy().getConfiguration(
						new ConfigurationProperties<>(AbstractDatabaseFeature.class).setVariantId(getDbType().toString()));
	}

	private void openConnection() throws SQLException {
		if (outsideConnection == null) {
			if (entitymanager == null) {
				throw new IllegalArgumentException("Must have connection configuration or entity manager");
			} else {
				return;
			}
		}
		currentConnection = outsideConnection;
		if (currentConnection == null) {
			try {
				String url = (String) InstallerRegistry.getInstance().get(InstallerRegistry.CONNECTION_URL);
				String user = (String) InstallerRegistry.getInstance().get(InstallerRegistry.CONNECTION_USER);
				String password = (String) InstallerRegistry.getInstance().get(InstallerRegistry.CONNECTION_PASSWORD);

				currentConnection = DriverManager.getConnection(url, user, password);
				currentConnection.setAutoCommit(false);
				initDbmsType();
			} catch (SQLException e) {
				ServiceRegistry.getLogProxy().error(this, "Could not open DB connection", e);
				throw e;
			}
		}
	}

	private void closeConnection() throws SQLException {
		if (outsideConnection == null && currentConnection != null) {
			try {
				currentConnection.close();
				currentConnection = outsideConnection;
				initDbmsType();
			} catch (SQLException e) {
				ServiceRegistry.getLogProxy().error(this, "Could not close DB connection", e);
				throw e;
			}
		}
	}

	protected ResultSet executeQuery(String query) throws SQLException {

		try {
			Statement dbStatement = currentConnection.createStatement();

			try {
				return dbStatement.executeQuery(query);
			} finally {
				dbStatement.close();
			}

		} catch (SQLException e) {
			ServiceRegistry.getLogProxy().error(this, "Could not execute query: {0}", e, query);
			throw e;
		}
	}

	protected int executeUpdate(String query) throws SQLException {

		try {
			Statement dbStatement = currentConnection.createStatement();

			try {
				return dbStatement.executeUpdate(query);
			} finally {
				dbStatement.close();
			}

		} catch (SQLException e) {
			ServiceRegistry.getLogProxy().error(this, "Could not execute update/insert/delete: {0}", e, query);
			throw e;
		}
	}

	protected boolean executeStatement(String statement) throws SQLException {

		try {
			Statement dbStatement = currentConnection.createStatement();

			try {
				return dbStatement.execute(statement);
			} finally {
				dbStatement.close();
			}

		} catch (SQLException e) {
			ServiceRegistry.getLogProxy().error(this, "Could not execute statement: {0}", e, statement);
			throw e;
		}
	}

	public String getSchema() throws SQLException {
		return currentConnection.getCatalog();
	}

	public DbmsType getDbType() {
		return dbType;
	}

	protected List<String> getTableColumns(String tableName) {
		if (tableName == null)
			return null;
		List<String> columns = new LinkedList<>();

		String statement = statements.getStringValue(TABLE_COLUMNS_KEY);
		statement = MessageFormat.format(statement, tableName);

		Connection connection = outsideConnection;
		if (connection == null) {
			try {
				String url = (String) InstallerRegistry.getInstance().get(InstallerRegistry.CONNECTION_URL);
				String user = (String) InstallerRegistry.getInstance().get(InstallerRegistry.CONNECTION_USER);
				String password = (String) InstallerRegistry.getInstance().get(InstallerRegistry.CONNECTION_PASSWORD);

				connection = DriverManager.getConnection(url, user, password);
				connection.setAutoCommit(false);
				initDbmsType();
			} catch (SQLException e) {
				ServiceRegistry.getLogProxy().error(this, "Could not open DB connection", e);
				return null;
			}
		}

		try {
			Statement dbStatement = connection.createStatement();

			ResultSet resultSet = dbStatement.executeQuery(statement);

			while (resultSet.next()) {
				// column indices are 1-based
				columns.add(resultSet.getString(1));
			}

		} catch (SQLException e) {
			ServiceRegistry.getLogProxy().error(this, "Could not execute statement: {0}", e, statement);
			return null;
		} finally {
			if (outsideConnection == null) {
				try {
					connection.close();
					dbType = null;
				} catch (SQLException e) {
					ServiceRegistry.getLogProxy().error(this, "Could not close DB connection", e);
					return null;
				}
			}
		}

		return columns;
	}

	/**
	 * @return the currentConnection
	 */
	public Connection getCurrentConnection() {
		return outsideConnection;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected final FeatureResult preInstallCheck() {
		ServiceRegistry.getLogProxy().debug(this, "Starting pre-install check: {0}", getAction());
		try {
			openConnection();
			try {
				return doPreInstallCheck();
			} finally {
				closeConnection();
				ServiceRegistry.getLogProxy().info(this, "Completed pre-install check: {0}", getAction());
			}
		} catch (SQLException e) {
			FeatureResult result = new FeatureResult(getAction());
			result.addProblem(
							new Problem(this).setAction("Pre-Install").setDescription(e.getLocalizedMessage()).setThrowable(e));
			return result;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected final FeatureResult performInstall() {
		ServiceRegistry.getLogProxy().debug(this, "Starting install: {0}", getAction());
		try {
			openConnection();
			try {
				return doPerformInstall();
			} finally {
				closeConnection();
				ServiceRegistry.getLogProxy().info(this, "Completed install: {0}", getAction());
			}
		} catch (SQLException e) {
			FeatureResult result = new FeatureResult(getAction());
			result.addProblem(new Problem(this).setAction("Install").setDescription(e.getLocalizedMessage()).setThrowable(e));
			return result;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected final FeatureResult postInstallCheck() {
		ServiceRegistry.getLogProxy().debug(this, "Starting post-install check: {0}", getAction());
		try {
			openConnection();
			try {
				return doPostInstallCheck();
			} finally {
				closeConnection();
				ServiceRegistry.getLogProxy().info(this, "Completed post-install check: {0}", getAction());
			}
		} catch (SQLException e) {
			FeatureResult result = new FeatureResult(getAction());
			result.addProblem(
							new Problem(this).setAction("Post-Install").setDescription(e.getLocalizedMessage()).setThrowable(e));
			return result;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected final FeatureResult performUninstall() {
		ServiceRegistry.getLogProxy().debug(this, "Starting uninstall: {0}", getAction());
		try {
			openConnection();
			try {
				return doPerformUninstall();
			} finally {
				closeConnection();
				ServiceRegistry.getLogProxy().info(this, "Completed uninstall: {0}", getAction());
			}
		} catch (SQLException e) {
			FeatureResult result = new FeatureResult(getAction());
			result.addProblem(
							new Problem(this).setAction("Uninstall").setDescription(e.getLocalizedMessage()).setThrowable(e));
			return result;
		}
	}

	protected abstract FeatureResult doPreInstallCheck();

	protected abstract FeatureResult doPerformInstall();

	protected abstract FeatureResult doPostInstallCheck();

	protected abstract FeatureResult doPerformUninstall();
}
