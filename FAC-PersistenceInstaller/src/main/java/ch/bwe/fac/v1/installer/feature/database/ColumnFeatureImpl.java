package ch.bwe.fac.v1.installer.feature.database;

import java.sql.Connection;
import java.util.List;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.util.Version;

/**
 * Default implementation of columns.
 *
 * @author Benjamin Weber
 *
 */
public class ColumnFeatureImpl extends AbstractColumnFeature {

	public ColumnFeatureImpl(Version version, String action, Connection connection, Environment... allowedEnvironments) {
		super(version, action, connection, allowedEnvironments);
	}

	public ColumnFeatureImpl(Version version, String action, Connection connection,
					List<Environment> allowedEnvironments) {
		super(version, action, connection, allowedEnvironments);
	}

}
