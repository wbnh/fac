package ch.bwe.fac.v1.installer.feature.database;

public enum DbmsType {

	MYSQL;

	@Override
	public String toString() {
		return name().toLowerCase();
	}
}
