package ch.bwe.fac.v1.installer.feature.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.Version;
import ch.bwe.fac.v1.installer.feature.FeatureResult;
import ch.bwe.fac.v1.installer.feature.FeatureResult.Problem;

/**
 * Feature to create tables on the DB.
 * 
 * @author benjamin
 */
public abstract class AbstractTableFeature extends AbstractDatabaseFeature {

  private static final int COMMENT_LENGTH = 1000;
  private static final String CREATE_KEY = "sql.table.create";
  private static final String CREATE_HISTORY_KEY = "sql.table.create.history";
  private static final String EXISTS_KEY = "sql.table.exists";
  private static final String DROP_KEY = "sql.table.drop";
  private static final String DROP_HISTORY_KEY = "sql.table.drop.history";
  /**
   * The suffix to use for history tables.
   */
  public static final String HISTORY_POSTFIX = "_H";

  private Configuration statements;

  private String tableName;
  private boolean createHistory;
  private List<AbstractColumnFeature> columns = new LinkedList<>();
  private List<AbstractColumnFeature> historyOnlyColumns = new LinkedList<>();

  /**
   * Constructor.
   * 
   * @param version             the version for which the feature is installed
   * @param action              the action we are performing
   * @param connection          the DB connection to use
   * @param allowedEnvironments the environments in which we install
   */
  protected AbstractTableFeature(Version version, String action, Connection connection,
      Environment... allowedEnvironments) {
    super(version, action, connection, allowedEnvironments);
  }

  /**
   * Constructor.
   * 
   * @param version             the version for which the feature is installed
   * @param action              the action we are performing
   * @param connection          the DB connection to use
   * @param allowedEnvironments the environments in which we install
   */
  protected AbstractTableFeature(Version version, String action, Connection connection,
      List<Environment> allowedEnvironments) {
    super(version, action, connection, allowedEnvironments);
  }

  /**
   * @param name the name of the table to create
   * @return this for chaining.
   */
  public AbstractTableFeature name(String name) {
    tableName = name;
    return this;
  }

  /**
   * Also create a history table.
   * 
   * @return this for chaining.
   */
  public AbstractTableFeature createHistory() {
    createHistory = true;
    return this;
  }

  /**
   * @param column the column to add
   * @return this for chaining.
   */
  public AbstractTableFeature column(AbstractColumnFeature column) {
    columns.add(column);
    return this;
  }

  /**
   * @param column the column to add for history only
   * @return this for chaining.
   */
  public AbstractTableFeature historyOnlyColumn(AbstractColumnFeature column) {
    historyOnlyColumns.add(column);
    return this;
  }

  private void loadStatements() {
    if (statements != null) { return; }

    statements = ServiceRegistry.getConfigurationProxy().getConfiguration(
        new ConfigurationProperties<>(AbstractTableFeature.class).setVariantId(getDbType().toString()));
  }

  @Override
  protected FeatureResult doPreInstallCheck() {
    loadStatements();
    final String action = "Check table nonexistence";
    FeatureResult result = new FeatureResult(getAction());

    try {
      String statement = MessageFormat.format(statements.getStringValue(EXISTS_KEY), getSchema(), tableName);
      ResultSet queryResult = executeQuery(statement);

      if (queryResult.next()) {
        result.addProblem(new Problem(this).setAction(action).setDescription("Table exists"));
      }

      if (createHistory) {
        statement = MessageFormat.format(statements.getStringValue(EXISTS_KEY), getSchema(),
            tableName + HISTORY_POSTFIX);
        queryResult = executeQuery(statement);

        if (queryResult.next()) {
          result.addProblem(new Problem(this).setAction(action).setDescription("History table exists"));
        }
      }
    } catch (SQLException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not check table existence", e);
      result.addProblem(new Problem(this).setAction(action).setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  @Override
  protected FeatureResult doPerformInstall() {
    loadStatements();
    FeatureResult result = new FeatureResult(getAction());

    try {
      String statement = MessageFormat.format(statements.getStringValue(CREATE_KEY), getSchema(), tableName,
          createColumnDefinitions(false), createPrimaryKeyDefinition(), createForeignKeyDefinitions(false));
      executeStatement(statement);

      if (createHistory) {
        statement = MessageFormat.format(statements.getStringValue(CREATE_HISTORY_KEY), getSchema(), tableName,
            createColumnDefinitions(true), createHistoryPrimaryKeyDefinition(), "");
        executeStatement(statement);
      }
    } catch (SQLException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not create table", e);
      result.addProblem(
          new Problem(this).setAction("Create table").setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  @Override
  protected FeatureResult doPostInstallCheck() {
    loadStatements();
    final String action = "Check table existence";
    FeatureResult result = new FeatureResult(getAction());

    try {
      String statement = MessageFormat.format(statements.getStringValue(EXISTS_KEY), getSchema(), tableName);
      ResultSet queryResult = executeQuery(statement);

      if (!queryResult.next()) {
        result.addProblem(new Problem(this).setAction(action).setDescription("Table does not exist"));
      }

      if (createHistory) {
        statement = MessageFormat.format(statements.getStringValue(EXISTS_KEY), getSchema(),
            tableName + HISTORY_POSTFIX);
        queryResult = executeQuery(statement);

        if (!queryResult.next()) {
          result.addProblem(new Problem(this).setAction(action).setDescription("History table does not exist"));
        }
      }
    } catch (SQLException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not check table existence", e);
      result.addProblem(new Problem(this).setAction(action).setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  @Override
  protected FeatureResult doPerformUninstall() {
    loadStatements();
    FeatureResult result = new FeatureResult(getAction());

    try {
      String statement = MessageFormat.format(statements.getStringValue(DROP_KEY), getSchema(), tableName);
      executeStatement(statement);

      if (createHistory) {
        statement = MessageFormat.format(statements.getStringValue(DROP_HISTORY_KEY), getSchema(), tableName);
        executeStatement(statement);
      }
    } catch (SQLException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not drop table", e);
      result.addProblem(
          new Problem(this).setAction("Drop table").setDescription(e.getLocalizedMessage()).setThrowable(e));
    }

    return result;
  }

  private String createColumnDefinitions(boolean history) {
    StringBuilder columnDefinitions = new StringBuilder();

    boolean compositePrimaryKey = isCompositePrimaryKey();

    List<AbstractColumnFeature> columnsToUse = new LinkedList<>(columns);
    if (history) {
      columnsToUse.addAll(historyOnlyColumns);
    }

    boolean first = true;
    for (AbstractColumnFeature column : columnsToUse) {
      if (history) {
        column.tableName(tableName + HISTORY_POSTFIX);
        column.referenceTablePostfix(HISTORY_POSTFIX);
      }

      if (first) {
        first = false;
      } else {
        columnDefinitions.append(", ");
      }
      columnDefinitions.append(column.getColumnDefinition(history, !compositePrimaryKey));

      column.referenceTablePostfix("");
      column.tableName(tableName);
    }

    return columnDefinitions.toString();
  }

  private boolean isCompositePrimaryKey() {
    boolean compositePrimaryKey = false;
    int primaryKeyCount = 0;
    for (AbstractColumnFeature column : columns) {
      if (column.isPrimaryKey()) {
        primaryKeyCount++;
      }
    }
    if (primaryKeyCount > 1) {
      compositePrimaryKey = true;
    }
    return compositePrimaryKey;
  }

  private String createPrimaryKeyDefinition() {
    StringBuilder primaryKeyDefinition = new StringBuilder();

    boolean first = true;
    for (AbstractColumnFeature column : columns) {
      if (column.isPrimaryKey()) {
        if (first) {
          first = false;
        } else {
          primaryKeyDefinition.append(", ");
        }
        primaryKeyDefinition.append(column.getName());
      }
    }

    return primaryKeyDefinition.toString();
  }

  private String createHistoryPrimaryKeyDefinition() {
    StringBuilder primaryKeyDefinition = new StringBuilder();

    boolean first = true;
    for (AbstractColumnFeature column : columns) {
      if (column.isHistoryPrimaryKey()) {
        if (first) {
          first = false;
        } else {
          primaryKeyDefinition.append(", ");
        }
        primaryKeyDefinition.append(column.getName());
      }
    }

    return primaryKeyDefinition.toString();
  }

  private String createForeignKeyDefinitions(boolean history) {
    StringBuilder foreignKeyDefinition = new StringBuilder();
    boolean compositePrimaryKey = isCompositePrimaryKey();

    for (AbstractColumnFeature column : columns) {
      if (column.isForeignKey()) {
        // comma is always appended
        foreignKeyDefinition.append(", ");
        foreignKeyDefinition.append(column.getForeignKeyDefinition(history, !compositePrimaryKey));
      }
    }

    return foreignKeyDefinition.toString();
  }

  /**
   * Adds all the audit fields as columns.
   */
  protected void addAuditFields() {
    addAuditFields(false);
  }

  /**
   * Adds all the audit fields as columns.
   */
  protected void addAuditFields(boolean skipRecordId) {

    if (!skipRecordId) {
      column(new ColumnFeatureImpl(getVersion(), getAction() + " recordId", getCurrentConnection(),
          getAllowedEnvironments()).name("recordId").type(Integer.class).nonNull().primaryKey().tableName(tableName));
    }
    column(new ColumnFeatureImpl(getVersion(), getAction() + " revisionId", getCurrentConnection(),
        getAllowedEnvironments()).name("revisionId").type(Integer.class).nonNull().historyPrimaryKey().defaultValue("0")
            .tableName(tableName));
    column(
        new ColumnFeatureImpl(getVersion(), getAction() + " creator", getCurrentConnection(), getAllowedEnvironments())
            .name("creator").type(Integer.class).tableName(tableName));
    column(new ColumnFeatureImpl(getVersion(), getAction() + " creationDateTime", getCurrentConnection(),
        getAllowedEnvironments()).name("creationDateTime").type(LocalDateTime.class).tableName(tableName));
    column(
        new ColumnFeatureImpl(getVersion(), getAction() + " modifier", getCurrentConnection(), getAllowedEnvironments())
            .name("modifier").type(Integer.class).tableName(tableName));
    column(new ColumnFeatureImpl(getVersion(), getAction() + " modificationDateTime", getCurrentConnection(),
        getAllowedEnvironments()).name("modificationDateTime").type(LocalDateTime.class).tableName(tableName));
    column(new ColumnFeatureImpl(getVersion(), getAction() + " deletedFlag", getCurrentConnection(),
        getAllowedEnvironments()).name("deletedFlag").type(Integer.class).tableName(tableName));
    column(new ColumnFeatureImpl(getVersion(), getAction() + " auditComment", getCurrentConnection(),
        getAllowedEnvironments()).name("auditComment").type(String.class).size(COMMENT_LENGTH).tableName(tableName));
    column(
        new ColumnFeatureImpl(getVersion(), getAction() + " revtype", getCurrentConnection(), getAllowedEnvironments())
            .name("revtype").type(Integer.class).tableName(tableName));
  }
}
