package ch.bwe.fac.v1.installer.feature;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.Level;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.Version;
import ch.bwe.fac.v1.installer.feature.FeatureResult.Problem;

public abstract class AbstractFeature {

	public static final String ENVIRONMENT_CHECK_PHASE = "Environment Check";
	public static final String PRE_INSTALL_CHECK_PHASE = "Pre-Install";
	public static final String INSTALL_PHASE = "Install";
	public static final String POST_INSTALL_CHECK_PHASE = "Post-Install";
	public static final String UNINSTALL_PHASE = "Uninstall";

	private String action;
	private Version version;
	private List<Environment> allowedEnvironments = new LinkedList<>();

	protected AbstractFeature(Version version, String action, Environment... allowedEnvironments) {
		this(version, action, Arrays.asList(allowedEnvironments));
	}

	protected AbstractFeature(Version version, String action, List<Environment> allowedEnvironments) {
		this.version = version;
		this.action = action;

		if (allowedEnvironments != null && !allowedEnvironments.isEmpty()) {
			this.allowedEnvironments.addAll(allowedEnvironments);
		}
	}

	protected FeatureResult environmentCheck() {
		Environment environment = ServiceRegistry.getEnvironment();

		FeatureResult result = new FeatureResult(action);

		if (!allowedEnvironments.contains(environment)) {
			Problem problem = new Problem(this);
			problem.setAction("Environment Check");
			problem.setDescription(MessageFormat.format("Current environment ({0}) is not allowed. Allowed are: {1}",
							environment, allowedEnvironments));
			result.addProblem(problem);
		}

		return result;
	}

	protected abstract FeatureResult preInstallCheck();

	protected abstract FeatureResult performInstall();

	protected abstract FeatureResult postInstallCheck();

	protected abstract FeatureResult performUninstall();

	public FeatureResult install() {
		return install(null);
	}

	public FeatureResult install(Version version) {
		if (version != null && this.version.compareTo(version) != 0) {
			FeatureResult result = new FeatureResult(action);
			result.addProblem(new Problem(this).setAction("version check"));
			return result;
		}

		ServiceRegistry.getLogProxy().debug(this, "Executing phase {0}...", ENVIRONMENT_CHECK_PHASE);
		FeatureResult environmentResult = environmentCheck();
		logFeatureResult(environmentResult, ENVIRONMENT_CHECK_PHASE);
		if (!environmentResult.isSuccessful()) {
			return environmentResult;
		}

		ServiceRegistry.getLogProxy().debug(this, "Executing phase {0}...", PRE_INSTALL_CHECK_PHASE);
		FeatureResult preInstallResult = preInstallCheck();
		logFeatureResult(preInstallResult, PRE_INSTALL_CHECK_PHASE);
		if (!preInstallResult.isSuccessful()) {
			return preInstallResult;
		}

		ServiceRegistry.getLogProxy().debug(this, "Executing phase {0}...", INSTALL_PHASE);
		FeatureResult installResult = performInstall();
		logFeatureResult(installResult, INSTALL_PHASE);
		if (!installResult.isSuccessful()) {
			return installResult;
		}

		ServiceRegistry.getLogProxy().debug(this, "Executing phase {0}...", POST_INSTALL_CHECK_PHASE);
		FeatureResult postInstallResult = postInstallCheck();
		logFeatureResult(postInstallResult, POST_INSTALL_CHECK_PHASE);
		if (!postInstallResult.isSuccessful()) {
			return postInstallResult;
		}

		return new FeatureResult(action);
	}

	public FeatureResult reinstall() {
		FeatureResult result = uninstall();
		if (!result.isSuccessful()) {
			logFeatureResult(result, "Re-Install");
			return result;
		}
		return install();
	}

	public FeatureResult uninstall() {
		return uninstall(null);
	}

	public FeatureResult uninstall(Version version) {
		if (version != null && this.version.compareTo(version) != 0) {
			FeatureResult result = new FeatureResult(action);
			result.addProblem(new Problem(this).setAction("version check"));
			return result;
		}

		ServiceRegistry.getLogProxy().debug(this, "Executing phase {0}...", ENVIRONMENT_CHECK_PHASE);
		FeatureResult environmentResult = environmentCheck();
		logFeatureResult(environmentResult, ENVIRONMENT_CHECK_PHASE);
		if (!environmentResult.isSuccessful()) {
			return environmentResult;
		}

		ServiceRegistry.getLogProxy().debug(this, "Executing phase {0}...", POST_INSTALL_CHECK_PHASE);
		FeatureResult postInstallResult = postInstallCheck();
		logFeatureResult(postInstallResult, POST_INSTALL_CHECK_PHASE);
		if (!postInstallResult.isSuccessful()) {
			return postInstallResult;
		}

		ServiceRegistry.getLogProxy().debug(this, "Executing phase {0}...", UNINSTALL_PHASE);
		FeatureResult uninstallResult = performUninstall();
		logFeatureResult(uninstallResult, UNINSTALL_PHASE);
		if (!uninstallResult.isSuccessful()) {
			return uninstallResult;
		}

		ServiceRegistry.getLogProxy().debug(this, "Executing phase {0}...", PRE_INSTALL_CHECK_PHASE);
		FeatureResult preInstallResult = preInstallCheck();
		logFeatureResult(preInstallResult, PRE_INSTALL_CHECK_PHASE);
		if (!preInstallResult.isSuccessful()) {
			return preInstallResult;
		}

		return new FeatureResult(action);
	}

	protected void logFeatureResult(FeatureResult result, String phase) {
		if (result.isSuccessful()) {
			ServiceRegistry.getLogProxy().info(this, "{1} | {0} executed successfully", result.getFeatureAction(), phase);

		} else {
			Level level = Level.WARN;
			if (ENVIRONMENT_CHECK_PHASE.equals(phase)) {
				level = Level.INFO;
			}

			List<Problem> problems = result.getProblems();
			ServiceRegistry.getLogProxy().log(level, this, "{2} | {0} encountered {1} problems during execution", null,
							result.getFeatureAction(), problems.size(), phase);

			for (Problem problem : problems) {
				Object executor = problem.getExecutor();
				if (executor == null) {
					executor = this;
				}
				if (problem.getThrowable() == null) {
					ServiceRegistry.getLogProxy().log(level, executor, " -- {0}: {1}", null, problem.getAction(),
									problem.getDescription());

				} else {
					ServiceRegistry.getLogProxy().error(executor, "Exception occurred: {0}: {1}", problem.getThrowable(),
									problem.getAction(), problem.getDescription());
				}
			}
		}
	}

	public String getAction() {
		return action;
	}

	public Version getVersion() {
		return version;
	}

	/**
	 * @return the allowedEnvironments
	 */
	public List<Environment> getAllowedEnvironments() {
		return allowedEnvironments;
	}
}
