package ch.bwe.fac.v1.installer.feature.database;

import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Test;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.util.Version;
import ch.bwe.fac.v1.test.impl.TestDatabasePersistable;

/**
 * Test class for model table features.
 *
 * @author Benjamin Weber
 */
public class TestClassModelTableFeature extends AbstractModelTableFeature {
  private static final String TEST_PRODUCT_NAME = "mysql";
  private static final String TEST_SCHEMA = "vierkant_dev";

  /**
   * Constructor handling initialization of mandatory fields.
   */
  public TestClassModelTableFeature() {
    super(new Version(0), "Test", TestDatabasePersistable.class, new TestConnection(TEST_PRODUCT_NAME, TEST_SCHEMA),
        Environment.ALL);
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testInstall() throws Exception {
    performInstall();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected boolean executeStatement(String statement) throws SQLException {
    boolean normal = "CREATE TABLE TestDatabasePersistable (id INT NOT NULL AUTO_INCREMENT , testString TEXT , testInteger INT ,CONSTRAINT PK_TestDatabasePersistable PRIMARY KEY (id));"
        .equals(statement);
    boolean history = "CREATE TABLE TestDatabasePersistable_H (id INT NOT NULL DEFAULT 0, testString TEXT , testInteger INT ,CONSTRAINT PK_TestDatabasePersistable_H PRIMARY KEY (id));"
        .equals(statement);

    Assert.assertTrue(statement, normal || history);
    return true;
  }
}
