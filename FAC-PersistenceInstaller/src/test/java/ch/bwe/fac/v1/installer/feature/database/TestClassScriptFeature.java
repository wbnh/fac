package ch.bwe.fac.v1.installer.feature.database;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.bwe.faa.v1.core.service.Environment;
import ch.bwe.faa.v1.core.util.Version;
import ch.bwe.fac.v1.installer.feature.FeatureResult;

/**
 * Test class to test the feature to create and delete the history triggers.
 *
 * @author Benjamin Weber
 *
 */
public class TestClassScriptFeature extends AbstractScriptFeature {
	private static final String TEST_PRODUCT_NAME = "mysql";
	private static final String TEST_SCHEMA = "vierkant_dev";

	private static final String PRE_INSTALL_TEXT = "PreInstallScript";
	private static final String INSTALL_TEXT = "InstallScript";
	private static final String POST_INSTALL_TEXT = "PostInstallScript";
	private static final String UNINSTALL_TEXT = "UninstallScript";

	private String expectedScript;

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public TestClassScriptFeature() {
		super(new Version(0), "Test", new TestConnection(TEST_PRODUCT_NAME, TEST_SCHEMA), Environment.ALL);

		if (Files.exists(Paths.get(INSTALL_TEXT + ".sql"))) {
			preCheckScript(Paths.get(PRE_INSTALL_TEXT + ".sql"));
			installScript(Paths.get(INSTALL_TEXT + ".sql"));
			postCheckScript(Paths.get(POST_INSTALL_TEXT + ".sql"));
			uninstallScript(Paths.get(UNINSTALL_TEXT + ".sql"));
		} else {
			preCheckScript(Paths.get("src", "test", "resources", PRE_INSTALL_TEXT + ".sql"));
			installScript(Paths.get("src", "test", "resources", INSTALL_TEXT + ".sql"));
			postCheckScript(Paths.get("src", "test", "resources", POST_INSTALL_TEXT + ".sql"));
			uninstallScript(Paths.get("src", "test", "resources", UNINSTALL_TEXT + ".sql"));
		}

	}

	@Before
	public void setUpTest() {
		expectedScript = null;
	}

	@Test
	public void testPreInstall() throws Exception {
		expectedScript = PRE_INSTALL_TEXT;
		FeatureResult result = preInstallCheck();
		Assert.assertTrue(result.isSuccessful());
	}

	@Test
	public void testInstall() throws Exception {
		expectedScript = INSTALL_TEXT;
		FeatureResult result = performInstall();
		Assert.assertTrue(result.isSuccessful());
	}

	@Test
	public void testPostInstall() throws Exception {
		expectedScript = POST_INSTALL_TEXT;
		FeatureResult result = postInstallCheck();
		Assert.assertTrue(result.isSuccessful());
	}

	@Test
	public void testUninstall() throws Exception {
		expectedScript = UNINSTALL_TEXT;
		FeatureResult result = performUninstall();
		Assert.assertTrue(result.isSuccessful());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String loadScript(Path script) throws IOException {
		String loadedScript = super.loadScript(script);

		checkScript(loadedScript);

		return "";
	}

	private void checkScript(String script) {
		Assert.assertNotNull(script);
		script = script.trim();
		Assert.assertEquals(expectedScript, script);
	}
}
