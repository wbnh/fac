package ch.bwe.fac.v1.type.restriction;

public class NullRestriction extends AbstractUnaryRestriction {

	public NullRestriction(String field) {
		super(field);
	}

	public NullRestriction(String field, boolean not) {
		super(field, not);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());

		builder.append("(");
		builder.append(getField());
		builder.append(" IS NULL)");

		return builder.toString();
	}

}
