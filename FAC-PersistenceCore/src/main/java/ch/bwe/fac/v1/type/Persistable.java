package ch.bwe.fac.v1.type;

import java.io.Serializable;
import java.util.Arrays;

import ch.bwe.faa.v1.core.service.ServiceRegistry;

/**
 * @author benjamin
 *
 */
public interface Persistable extends Serializable {

	/**
	 * Gets the string representation of the identifier of this persistable.
	 * 
	 * @return the string representation of the identifier of this persistable.
	 */
	default String getIdentifier() {
		StringBuilder anIdentifier = new StringBuilder();

		boolean[] first = { true };
		Arrays.stream(getClass().getFields()).forEach(f -> {
			f.setAccessible(true);
			if (!f.isAnnotationPresent(Attribute.class))
				return;
			if (!f.isAnnotationPresent(Identifier.class))
				return;

			if (first[0])
				first[0] = false;
			else
				anIdentifier.append("_");

			try {
				anIdentifier.append(f.get(this));
			} catch (Exception problem) {
				ServiceRegistry.getLogProxy().error(this, "Could not get identifier value for field ''{0}''", problem,
								f.getName());
			}
		});

		String outString = anIdentifier.toString();
		if (outString.isEmpty() || outString.matches("_\\s]*")) {
			return null;
		}

		return outString;
	}

}
