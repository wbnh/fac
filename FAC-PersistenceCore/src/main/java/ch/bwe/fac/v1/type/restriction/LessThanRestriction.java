package ch.bwe.fac.v1.type.restriction;

public class LessThanRestriction extends AbstractBinaryRestriction {

	public LessThanRestriction(String field, Object value) {
		super(field, value);
	}

	public LessThanRestriction(String field, Object value, boolean not) {
		super(field, value, not);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());

		builder.append("(");
		builder.append(getField());
		builder.append(" < ");
		builder.append(getValue());
		builder.append(")");

		return builder.toString();
	}

}
