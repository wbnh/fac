package ch.bwe.fac.v1.type.restriction;

public abstract class AbstractJunctionRestriction extends AbstractRestriction {

	private AbstractRestriction left;
	private AbstractRestriction right;

	protected AbstractJunctionRestriction(AbstractRestriction left, AbstractRestriction right) {
		super();

		this.left = left;
		this.right = right;
	}

	protected AbstractJunctionRestriction(AbstractRestriction left, AbstractRestriction right, boolean not) {
		super(not);

		this.left = left;
		this.right = right;
	}

	protected AbstractJunctionRestriction() {
	}

	protected AbstractJunctionRestriction(boolean not) {
		super(not);
	}

	public AbstractRestriction getLeft() {
		return left;
	}

	public void setLeft(AbstractRestriction left) {
		this.left = left;
	}

	public AbstractRestriction getRight() {
		return right;
	}

	public void setRight(AbstractRestriction right) {
		this.right = right;
	}
}
