package ch.bwe.fac.v1.type;

public class SaveException extends Exception {

	private static final long serialVersionUID = -646852426751187899L;

	public SaveException() {
	}

	public SaveException(String arg0) {
		super(arg0);
	}

	public SaveException(Throwable arg0) {
		super(arg0);
	}

	public SaveException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
