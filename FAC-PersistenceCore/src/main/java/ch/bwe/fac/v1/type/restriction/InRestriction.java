package ch.bwe.fac.v1.type.restriction;

import java.util.Arrays;
import java.util.Collection;

public class InRestriction extends AbstractBinaryRestriction {

	public InRestriction(String field, Object[] value) {
		super(field, value);
	}

	public InRestriction(String field, Collection<?> value) {
		this(field, value.toArray());
	}

	public InRestriction(String field, Object[] value, boolean not) {
		super(field, value, not);
	}

	public InRestriction(String field, Collection<?> value, boolean not) {
		this(field, value.toArray(), not);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());

		builder.append("(");
		builder.append(getField());
		builder.append(" IN ");
		builder.append(Arrays.toString((Object[]) getValue()));
		builder.append(")");

		return builder.toString();
	}

}
