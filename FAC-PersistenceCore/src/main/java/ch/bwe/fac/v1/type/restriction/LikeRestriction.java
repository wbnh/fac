package ch.bwe.fac.v1.type.restriction;

import java.util.List;

public class LikeRestriction extends AbstractBinaryRestriction {

	public LikeRestriction(String field, String value) {
		super(field, value);
	}

	public LikeRestriction(String field, String value, boolean not) {
		super(field, value, not);
	}

	public static AbstractRestriction create(String field, List<String> value) {
		if (value == null || value.isEmpty()) {
			return null;
		}
		if (value.size() == 1) {
			return new LikeRestriction(field, value.get(0));
		}

		AbstractJunctionRestriction root = new DisjunctionRestriction();
		root.setLeft(new LikeRestriction(field, value.get(0)));

		AbstractJunctionRestriction currentRestriction = root;
		for (int i = 1; i < value.size(); i++) {
			DisjunctionRestriction newRestriction = new DisjunctionRestriction();
			newRestriction.setLeft(new LikeRestriction(field, value.get(i)));
			currentRestriction.setRight(newRestriction);
			currentRestriction = newRestriction;
		}

		return root;
	}

	public static AbstractRestriction create(String field, List<String> value, boolean not) {
		if (value == null || value.isEmpty()) {
			return null;
		}
		if (value.size() == 1) {
			return new LikeRestriction(field, value.get(0));
		}

		AbstractJunctionRestriction root = new DisjunctionRestriction();
		root.setLeft(new LikeRestriction(field, value.get(0), not));

		AbstractJunctionRestriction currentRestriction = root;
		for (int i = 1; i < value.size(); i++) {
			DisjunctionRestriction newRestriction = new DisjunctionRestriction();
			newRestriction.setLeft(new LikeRestriction(field, value.get(i), not));
			currentRestriction.setRight(newRestriction);
			currentRestriction = newRestriction;
		}

		return root;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());

		builder.append("(");
		builder.append(getField());
		builder.append(" LIKE ");
		builder.append(getValue());
		builder.append(")");

		return builder.toString();
	}

}
