package ch.bwe.fac.v1.type;

public class DeleteException extends Exception {

	private static final long serialVersionUID = -2040568085039133224L;

	public DeleteException() {
	}

	public DeleteException(String arg0) {
		super(arg0);
	}

	public DeleteException(Throwable arg0) {
		super(arg0);
	}

	public DeleteException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
