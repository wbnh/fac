package ch.bwe.fac.v1.type.restriction;

public abstract class AbstractBinaryRestriction extends AbstractRestriction {

	private String field;
	private Object value;

	protected AbstractBinaryRestriction(String field, Object value) {
		super();

		this.field = field;
		this.value = value;
	}

	protected AbstractBinaryRestriction(String field, Object value, boolean not) {
		super(not);

		this.field = field;
		this.value = value;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
