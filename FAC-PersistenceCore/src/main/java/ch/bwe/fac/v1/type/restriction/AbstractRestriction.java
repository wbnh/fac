package ch.bwe.fac.v1.type.restriction;

public abstract class AbstractRestriction {

	private boolean not;

	protected AbstractRestriction() {
		this(false);
	}

	protected AbstractRestriction(boolean not) {
		this.not = not;
	}

	public boolean isNot() {
		return not;
	}

	@Override
	public String toString() {
		if (not) {
			return "~";
		} else {
			return "";
		}
	}
}
