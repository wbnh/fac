package ch.bwe.fac.v1.type.restriction;

public abstract class AbstractUnaryRestriction extends AbstractRestriction {

	private String field;

	protected AbstractUnaryRestriction(String field) {
		super();

		this.field = field;
	}

	protected AbstractUnaryRestriction(String field, boolean not) {
		super(not);

		this.field = field;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

}
