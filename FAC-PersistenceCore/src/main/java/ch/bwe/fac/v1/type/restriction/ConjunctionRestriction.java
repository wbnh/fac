package ch.bwe.fac.v1.type.restriction;

public class ConjunctionRestriction extends AbstractJunctionRestriction {

	public ConjunctionRestriction(AbstractRestriction left, AbstractRestriction right) {
		super(left, right);
	}

	public ConjunctionRestriction() {
	}

	public ConjunctionRestriction(AbstractRestriction left, AbstractRestriction right, boolean not) {
		super(left, right, not);
	}

	public ConjunctionRestriction(boolean not) {
		super(not);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());
		
		builder.append("(");
		builder.append(getLeft().toString());
		builder.append(" AND ");
		builder.append(getRight().toString());
		builder.append(")");
		
		return builder.toString();
	}
}
