package ch.bwe.fac.v1.type.restriction;

public class LessEqualRestriction extends AbstractBinaryRestriction {

	public LessEqualRestriction(String field, Object value) {
		super(field, value);
	}

	public LessEqualRestriction(String field, Object value, boolean not) {
		super(field, value, not);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());

		builder.append("(");
		builder.append(getField());
		builder.append(" <= ");
		builder.append(getValue());
		builder.append(")");

		return builder.toString();
	}

}
