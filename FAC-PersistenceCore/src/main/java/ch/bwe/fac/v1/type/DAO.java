package ch.bwe.fac.v1.type;

import java.util.List;

import ch.bwe.faa.v1.core.util.Pair;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;

/**
 * A data access object.
 * 
 * @param <T> the type of persistable to access
 * @param <U> the type of the history persistable
 * @author benjamin
 */
public interface DAO<T extends Persistable> {

  /**
   * Saves the passed persistable and returns an updated version of it.
   * 
   * @param inPersistable the persistable to save
   * @param modifier      the person who modified the record
   * @return the updated version of the persistable
   */
  T save(T inPersistable, Integer modifier) throws SaveException;

  /**
   * Deletes the passed persistable.
   * 
   * @param inPersistable the persistable to delete
   * @throws DeleteException if we could not delete
   */
  void delete(Integer recordId) throws DeleteException;

  void remove(Integer recordId) throws DeleteException;

  /**
   * Loads the revision IDs for the passed model.
   * 
   * @param recordId the ID of the model to get revisions of
   * @return the revision IDs
   */
  List<Integer> loadEnversRevisionIds(Integer recordId);

  /**
   * Loads the persistable with the passed identifier.
   * 
   * @param inIdentifier the ID of the persistable to load
   * @return the loaded persistable
   */
  T load(Integer inIdentifier);

  /**
   * Loads the persistable from the history table with the passed identifier.
   * 
   * @param recordId   the record id
   * @param revisionId the revision id
   * @return the loaded revision
   */
  T loadHistory(Integer recordId, Integer revisionId);

  /**
   * Loads the persistable from the history table with the passed identifier.
   * 
   * @param recordId   the record id
   * @param revisionId the revision id
   * @return the loaded revision
   */
  T loadHistoryEnvers(Integer recordId, Integer revisionId);

  List<T> find(AbstractRestriction restriction, int startIndex, int count);

  List<T> find(AbstractRestriction restriction, int startIndex, int count, List<Pair<String, Boolean>> orders);

  /**
   * Gets all IDs of the valid objects in the database.
   * 
   * @return the object IDs
   */
  List<Integer> getAllIds();

  /**
   * Gets all IDs of the valid objects in the specified range.
   * 
   * @param startIndex the starting index
   * @param count      the amount of items
   * @return the IDs
   */
  List<Integer> getAllIds(int startIndex, int count);

  /**
   * Gets all IDs of the valid objects in the specified range.
   * 
   * @param startIndex the starting index
   * @param count      the amount of items
   * @return the IDs
   */
  List<Integer> getAllIds(AbstractRestriction restriction, int startIndex, int count);

  /**
   * Gets all IDs of the valid objects in the specified range.
   * 
   * @param startIndex the starting index
   * @param count      the amount of items
   * @return the IDs
   */
  List<Integer> getAllIds(AbstractRestriction restriction, int startIndex, int count,
      List<Pair<String, Boolean>> orders);

  /**
   * Counts the valid entries.
   * 
   * @return the count
   */
  Long count();

  /**
   * Counts the valid entries.
   * 
   * @return the count
   */
  Long count(AbstractRestriction restriction);

  /**
   * Checks whether the passed ID exists.
   * 
   * @param identifier the ID of the object to check
   * @return <code>true</code> if the ID exists, <code>false</code> otherwise
   */
  boolean containsId(Integer identifier);

  /**
   * Checks whether the passed ID exists.
   * 
   * @param identifier the ID of the object to check
   * @return <code>true</code> if the ID exists, <code>false</code> otherwise
   */
  boolean containsId(AbstractRestriction restriction, Object identifier);

  /**
   * Loads the value of a single column for a single row on the database.
   * 
   * @param recordId   the ID of the model to load
   * @param propertyId the column name
   * @return the value
   */
  Object getProperty(Integer recordId, String propertyId);

  Long getPersistableIndex(Integer recordId);

  Long getPersistableIndex(AbstractRestriction restriction, Integer recordId);

  Long getPersistableIndex(AbstractRestriction restriction, Integer recordId, List<Pair<String, Boolean>> orders);

  Integer getFirstId();

  Integer getFirstId(AbstractRestriction restriction);

  Integer getFirstId(AbstractRestriction restriction, List<Pair<String, Boolean>> orders);

  Integer getLastId();

  Integer getLastId(AbstractRestriction restriction);

  Integer getLastId(AbstractRestriction restriction, List<Pair<String, Boolean>> orders);

  Integer getNextId(Integer recordId);

  Integer getNextId(AbstractRestriction restriction, Integer recordId);

  Integer getNextId(AbstractRestriction restriction, Integer recordId, List<Pair<String, Boolean>> orders);

  Integer getPreviousId(Integer recordId);

  Integer getPreviousId(AbstractRestriction restriction, Integer recordId);

  Integer getPreviousId(AbstractRestriction restriction, Integer recordId, List<Pair<String, Boolean>> orders);
}
