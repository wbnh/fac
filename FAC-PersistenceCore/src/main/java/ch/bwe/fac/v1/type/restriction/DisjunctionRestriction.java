package ch.bwe.fac.v1.type.restriction;

public class DisjunctionRestriction extends AbstractJunctionRestriction {

	public DisjunctionRestriction(AbstractRestriction left, AbstractRestriction right) {
		super(left, right);
	}

	public DisjunctionRestriction() {
	}

	public DisjunctionRestriction(AbstractRestriction left, AbstractRestriction right, boolean not) {
		super(left, right, not);
	}

	public DisjunctionRestriction(boolean not) {
		super(not);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());
		
		builder.append("(");
		builder.append(getLeft().toString());
		builder.append(" OR ");
		builder.append(getRight().toString());
		builder.append(")");
		
		return builder.toString();
	}
}
