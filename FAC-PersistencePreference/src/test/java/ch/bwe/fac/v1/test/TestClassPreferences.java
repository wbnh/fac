package ch.bwe.fac.v1.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ch.bwe.fac.v1.test.impl.TestPreferenceDAO;
import ch.bwe.fac.v1.test.impl.TestPreferencePersistable;

/**
 * @author benjamin
 *
 */
public class TestClassPreferences {

	@Test
	public void testSave() throws Exception {
		TestPreferencePersistable aTestPreferencePersistable = new TestPreferencePersistable();

		TestPreferenceDAO aDao = new TestPreferenceDAO();

		aDao.save(aTestPreferencePersistable);

		TestPreferencePersistable aLoad = aDao.load(aTestPreferencePersistable.id);

		assertEquals(aTestPreferencePersistable, aLoad);
	}

}
