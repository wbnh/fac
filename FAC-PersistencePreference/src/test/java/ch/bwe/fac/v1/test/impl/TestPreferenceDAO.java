package ch.bwe.fac.v1.test.impl;

import ch.bwe.fac.v1.preferences.AbstractPreferenceDAO;
import ch.bwe.fac.v1.type.DeleteException;
import ch.bwe.fac.v1.type.SaveException;

/**
 * @author benjamin
 */
public class TestPreferenceDAO extends AbstractPreferenceDAO<TestPreferencePersistable> {

	/**
	 * Constructor.
	 */
	public TestPreferenceDAO() {
		super("FAC-PrefTest", TestPreferencePersistable.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TestPreferencePersistable loadHistory(Integer recordId, Integer revisionId) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsId(Integer identifier) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TestPreferencePersistable save(TestPreferencePersistable inPersistable, Integer modifier)
					throws SaveException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove(Integer recordId) throws DeleteException {
		// TODO Auto-generated method stub

	}

}
