package ch.bwe.fac.v1.test.impl;

import java.util.Objects;

import ch.bwe.fac.v1.preferences.AbstractPreferencePersistable;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.DAO;
import ch.bwe.fac.v1.type.Identifier;
import ch.bwe.fac.v1.type.Persistable;

/**
 * @author benjamin
 */
public class TestPreferencePersistable extends AbstractPreferencePersistable {

	/**
	 * SUID.
	 */
	private static final long serialVersionUID = 53879521886746580L;

	@Attribute
	@Identifier
	public int id = 5;

	@Attribute
	public String testString = "testStringValue";

	@Attribute
	public Integer testInteger = 2;

	/**
	 * Constructor.
	 */
	public TestPreferencePersistable() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public DAO<Persistable, Persistable> getAssociatedDAO() {
		DAO<?, ?> outDAO = new TestPreferenceDAO();

		return (DAO<Persistable, Persistable>) outDAO;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}

		if (obj instanceof TestPreferencePersistable) {
			TestPreferencePersistable other = (TestPreferencePersistable) obj;
			boolean equals = Objects.equals(id, other.id);
			equals &= Objects.equals(testString, other.testString);
			equals &= Objects.equals(testInteger, other.testInteger);

			return equals;
		}

		return false;
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode += Objects.hashCode(id) * 5;
		hashCode += Objects.hashCode(testString) * 7;
		hashCode += Objects.hashCode(testInteger) * 11;

		return hashCode;
	}
}
