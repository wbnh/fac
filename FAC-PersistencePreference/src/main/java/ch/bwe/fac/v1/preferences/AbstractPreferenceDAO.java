package ch.bwe.fac.v1.preferences;

import java.lang.instrument.IllegalClassFormatException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.DAO;
import ch.bwe.fac.v1.type.Persistable;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;

/**
 * @param <T>
 *          the type to access
 * @author benjamin
 */
public abstract class AbstractPreferenceDAO<T extends AbstractPreferencePersistable> implements DAO<T> {

	/**
	 * The last used id.
	 */
	private static int _lastUsedId = -1;

	/**
	 * The application name.
	 */
	private final String appName;

	/**
	 * The class of the persistable for reflection capabilities.
	 */
	private final Class<T> persistableClass;

	/**
	 * Constructor.
	 *
	 * @param appName
	 *          the application name
	 * @param persistableClass
	 *          the class of the persistable for reflection capabilities.
	 */
	protected AbstractPreferenceDAO(String appName, Class<T> persistableClass) {
		super();

		AssertUtils.notNull(appName);
		AssertUtils.notNull(persistableClass);

		this.appName = appName;
		this.persistableClass = persistableClass;
	}

	/**
	 * Gets the node for the passed persistable.
	 *
	 * @param inPersistable
	 *          the persistable
	 * @return the node
	 */
	private Preferences getPersistableNode(T inPersistable) {
		Preferences aPersistableRoot = getPersistableRoot(inPersistable.getClass().getName());

		String anIdentifier;

		{
			String aTempID = inPersistable.getIdentifier();
			if (aTempID == null) {
				aTempID = Integer.toString(++_lastUsedId);
			}

			anIdentifier = aTempID;
		}

		return aPersistableRoot.node(anIdentifier);
	}

	private Preferences getPersistableRoot(String inPersistableName) {
		Preferences aRoot = Preferences.userRoot().node(appName);
		return aRoot.node(inPersistableName);
	}

	/**
	 * {@inheritDoc}
	 */
	public T save(T inPersistable) {

		Preferences aPersistableNode = getPersistableNode(inPersistable);

		Arrays.stream(inPersistable.getClass().getFields()).filter(f -> {
			f.setAccessible(true);
			return f.isAnnotationPresent(Attribute.class);
		}).forEach(f -> {
			f.setAccessible(true);
			try {
				Class<?> aType = f.getType();
				if (String.class.isAssignableFrom(aType)) {
					aPersistableNode.put(f.getName(), (String) f.get(inPersistable));
				} else if (Integer.class.isAssignableFrom(aType)) {
					aPersistableNode.putInt(f.getName(), (Integer) f.get(inPersistable));
				} else if (Integer.TYPE.isAssignableFrom(aType)) {
					aPersistableNode.putInt(f.getName(), (int) f.get(inPersistable));
				} else if (Boolean.class.isAssignableFrom(aType)) {
					aPersistableNode.putBoolean(f.getName(), (Boolean) f.get(inPersistable));
				} else if (Boolean.TYPE.isAssignableFrom(aType)) {
					aPersistableNode.putBoolean(f.getName(), (boolean) f.get(inPersistable));
				} else if (Double.class.isAssignableFrom(aType)) {
					aPersistableNode.putDouble(f.getName(), (Double) f.get(inPersistable));
				} else if (Double.TYPE.isAssignableFrom(aType)) {
					aPersistableNode.putDouble(f.getName(), (double) f.get(inPersistable));
				} else if (Float.class.isAssignableFrom(aType)) {
					aPersistableNode.putFloat(f.getName(), (Float) f.get(inPersistable));
				} else if (Float.TYPE.isAssignableFrom(aType)) {
					aPersistableNode.putFloat(f.getName(), (float) f.get(inPersistable));
				} else if (Long.class.isAssignableFrom(aType)) {
					aPersistableNode.putLong(f.getName(), (Long) f.get(inPersistable));
				} else if (Long.TYPE.isAssignableFrom(aType)) {
					aPersistableNode.putLong(f.getName(), (long) f.get(inPersistable));
				} else if (Persistable.class.isAssignableFrom(aType)) {
					Persistable aPersistable = (Persistable) f.get(inPersistable);
					Persistable anUpdatedSub = aPersistable.getAssociatedDAO().save(aPersistable, 0);
					f.set(inPersistable, anUpdatedSub);
				} else {
					throw new IllegalClassFormatException(
									"Attributes must be of type String, Integer, boolean, Double, Float, Long or Persistable");
				}

				aPersistableNode.flush();
			} catch (Exception problem) {
				ServiceRegistry.getLogProxy().error(this, "Could not save Persistable {0}", problem, aPersistableNode.name());
				throw new RuntimeException(problem);
			}
		});

		return inPersistable;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Integer recordId) {
		T inPersistable = load(recordId);
		Preferences aNode = getPersistableNode(inPersistable);

		try {
			aNode.removeNode();

			aNode.flush();
		} catch (BackingStoreException problem) {
			ServiceRegistry.getLogProxy().error(this, "Could not remove persistable {0}", problem, aNode.name());
			throw new RuntimeException(problem);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T load(Integer inIdentifier) {
		String aPersistableName = persistableClass.getName();

		Preferences aPersistableRoot = getPersistableRoot(aPersistableName);

		try {
			if (aPersistableRoot.nodeExists(inIdentifier.toString())) {
				Preferences aNode = aPersistableRoot.node(inIdentifier.toString());

				T outInstance = persistableClass.getConstructor().newInstance();

				for (String aCurrentKey : aNode.keys()) {
					Field aField = persistableClass.getField(aCurrentKey);
					aField.setAccessible(true);
					Class<?> aCurrentType = aField.getType();
					if (String.class.isAssignableFrom(aCurrentType)) {
						aField.set(outInstance, aNode.get(aCurrentKey, null));
					} else if (Integer.TYPE.isAssignableFrom(aCurrentType)) {
						aField.set(outInstance, aNode.getInt(aCurrentKey, 0));
					} else if (Integer.class.isAssignableFrom(aCurrentType)) {
						aField.set(outInstance, aNode.getInt(aCurrentKey, 0));
					} else if (Boolean.TYPE.isAssignableFrom(aCurrentType)) {
						aField.set(outInstance, aNode.getBoolean(aCurrentKey, false));
					} else if (Boolean.class.isAssignableFrom(aCurrentType)) {
						aField.set(outInstance, aNode.getBoolean(aCurrentKey, false));
					} else if (Double.TYPE.isAssignableFrom(aCurrentType)) {
						aField.set(outInstance, aNode.getDouble(aCurrentKey, 0d));
					} else if (Double.class.isAssignableFrom(aCurrentType)) {
						aField.set(outInstance, aNode.getDouble(aCurrentKey, 0d));
					} else if (Float.TYPE.isAssignableFrom(aCurrentType)) {
						aField.set(outInstance, aNode.getFloat(aCurrentKey, 0f));
					} else if (Float.class.isAssignableFrom(aCurrentType)) {
						aField.set(outInstance, aNode.getFloat(aCurrentKey, 0f));
					} else if (Long.TYPE.isAssignableFrom(aCurrentType)) {
						aField.set(outInstance, aNode.getLong(aCurrentKey, 0L));
					} else if (Long.class.isAssignableFrom(aCurrentType)) {
						aField.set(outInstance, aNode.getLong(aCurrentKey, 0L));
					}
				}

				return outInstance;
			}
		} catch (BackingStoreException | InstantiationException | IllegalAccessException | NoSuchFieldException
						| SecurityException | InvocationTargetException | NoSuchMethodException problem) {
			ServiceRegistry.getLogProxy().error(this, "Could not load persistable {0}", problem, inIdentifier);
			throw new RuntimeException(problem);
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Integer> getAllIds() {
		throw new RuntimeException("not implemented");
	}

	public List<Integer> getAllIds(int startIndex, int count) {
		throw new RuntimeException("not implemented");
	}

	/**
	 * {@inheritDoc}
	 */
	public Long count() {
		throw new RuntimeException("not implemented");
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean containsId(Object identifier) {
		throw new RuntimeException("not implemented");
	}

	@Override
	public Object getProperty(Integer recordId, String propertyId) {
		throw new RuntimeException("not implemented");
	}

	public Long getPersistableIndex(Integer recordId) {
		throw new RuntimeException("not implemented");
	}

	public Integer getFirstId() {
		throw new RuntimeException("not implemented");
	}

	public Integer getLastId() {
		throw new RuntimeException("not implemented");
	}

	public Integer getNextId(Integer recordId) {
		throw new RuntimeException("not implemented");
	}

	public Integer getPreviousId(Integer recordId) {
		throw new RuntimeException("not implemented");
	}

	@Override
	public List<Integer> getAllIds(AbstractRestriction restriction, int startIndex, int count) {
		throw new RuntimeException("not implemented");
	}

	@Override
	public Long count(AbstractRestriction restriction) {
		throw new RuntimeException("not implemented");
	}

	@Override
	public boolean containsId(AbstractRestriction restriction, Object identifier) {
		throw new RuntimeException("not implemented");
	}

	@Override
	public Long getPersistableIndex(AbstractRestriction restriction, Integer recordId) {
		throw new RuntimeException("not implemented");
	}

	@Override
	public Integer getFirstId(AbstractRestriction restriction) {
		throw new RuntimeException("not implemented");
	}

	@Override
	public Integer getLastId(AbstractRestriction restriction) {
		throw new RuntimeException("not implemented");
	}

	@Override
	public Integer getNextId(AbstractRestriction restriction, Integer recordId) {
		throw new RuntimeException("not implemented");
	}

	@Override
	public Integer getPreviousId(AbstractRestriction restriction, Integer recordId) {
		throw new RuntimeException("not implemented");
	}

	@Override
	public List<T> find(AbstractRestriction restriction, int startIndex, int count) {
		throw new RuntimeException("not implemented");
	}
}
