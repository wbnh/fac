package ch.bwe.fac.v1.preferences;

import ch.bwe.fac.v1.type.Persistable;

/**
 * @author benjamin
 *
 */
public abstract class AbstractPreferencePersistable implements Persistable {

	/**
	 * SUID.
	 */
	private static final long serialVersionUID = 2313873022839842105L;

	/**
	 * Constructor.
	 */
	protected AbstractPreferencePersistable() {
		super();
	}
}
