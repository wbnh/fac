package ch.bwe.fac.v1.test.impl;

import java.time.LocalDateTime;
import java.util.Objects;

import ch.bwe.fac.v1.database.AbstractDatabasePersistable;
import ch.bwe.fac.v1.database.DatabaseModel;
import ch.bwe.fac.v1.type.Attribute;
import ch.bwe.fac.v1.type.Identifier;

/**
 * @author benjamin
 */
public class TestDatabasePersistable extends AbstractDatabasePersistable implements DatabaseModel {

	/**
	 * SUID.
	 */
	private static final long serialVersionUID = 53879521886746580L;

	@Attribute
	@Identifier
	public int id = 5;

	@Attribute(length = 10)
	public String testString = "testStringValue";

	@Attribute
	public Integer testInteger = 2;

	/**
	 * Constructor.
	 */
	public TestDatabasePersistable() {
		super();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}

		if (obj instanceof TestDatabasePersistable) {
			TestDatabasePersistable other = (TestDatabasePersistable) obj;
			boolean equals = Objects.equals(id, other.id);
			equals &= Objects.equals(testString, other.testString);
			equals &= Objects.equals(testInteger, other.testInteger);

			return equals;
		}

		return false;
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode += Objects.hashCode(id) * 5;
		hashCode += Objects.hashCode(testString) * 7;
		hashCode += Objects.hashCode(testInteger) * 11;

		return hashCode;
	}

	@Override
	public Integer getRecordId() {
		return id;
	}

	@Override
	public void setRecordId(Integer recordId) {
		id = recordId;
	}

	@Override
	public Integer getRevisionId() {
		return id;
	}

	@Override
	public void setRevisionId(Integer revisionId) {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getCreator() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCreator(Integer creator) {
		// TODO Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LocalDateTime getCreationDateTime() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCreationDateTime(LocalDateTime creationDateTime) {
		// TODO Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getModifier() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setModifier(Integer modifier) {
		// TODO Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LocalDateTime getModificationDateTime() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setModificationDateTime(LocalDateTime modificationDateTime) {
		// TODO Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean getDeletedFlag() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDeletedFlag(boolean deletedFlag) {
		// TODO Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAuditComment() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAuditComment(String auditComment) {
		// TODO Auto-generated method stub

	}
}
