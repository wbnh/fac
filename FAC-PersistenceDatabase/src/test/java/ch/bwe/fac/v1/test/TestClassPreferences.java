package ch.bwe.fac.v1.test;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

import ch.bwe.fac.v1.test.impl.TestDatabaseDAO;
import ch.bwe.fac.v1.test.impl.TestDatabasePersistable;

/**
 * @author benjamin
 *
 */
public class TestClassPreferences {

	@Test
	@Ignore
	public void testSave() throws Exception {
		TestDatabasePersistable aTestPreferencePersistable = new TestDatabasePersistable();

		TestDatabaseDAO aDao = new TestDatabaseDAO();

		aDao.save(aTestPreferencePersistable, 0);

		TestDatabasePersistable aLoad = aDao.load(aTestPreferencePersistable.id);

		assertEquals(aTestPreferencePersistable, aLoad);
	}

}
