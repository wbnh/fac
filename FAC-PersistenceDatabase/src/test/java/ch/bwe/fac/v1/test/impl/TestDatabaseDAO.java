package ch.bwe.fac.v1.test.impl;

import ch.bwe.fac.v1.database.AbstractDatabaseDAO;

/**
 * @author benjamin
 */
public class TestDatabaseDAO extends AbstractDatabaseDAO<TestDatabasePersistable> {

  /**
   * Constructor.
   */
  public TestDatabaseDAO() {
    super(TestDatabasePersistable.class, "");
  }

}
