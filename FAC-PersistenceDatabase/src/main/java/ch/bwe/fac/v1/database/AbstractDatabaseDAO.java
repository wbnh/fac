package ch.bwe.fac.v1.database;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OptimisticLockException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.query.criteria.internal.OrderImpl;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.faa.v1.core.util.Pair;
import ch.bwe.fac.v1.type.DAO;
import ch.bwe.fac.v1.type.DeleteException;
import ch.bwe.fac.v1.type.SaveException;
import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.ConjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.DisjunctionRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.bwe.fac.v1.type.restriction.GreaterEqualRestriction;
import ch.bwe.fac.v1.type.restriction.GreaterThanRestriction;
import ch.bwe.fac.v1.type.restriction.InRestriction;
import ch.bwe.fac.v1.type.restriction.LessEqualRestriction;
import ch.bwe.fac.v1.type.restriction.LessThanRestriction;
import ch.bwe.fac.v1.type.restriction.LikeRestriction;
import ch.bwe.fac.v1.type.restriction.NullRestriction;

/**
 * @param <T> the type to access
 * @author benjamin
 */
public abstract class AbstractDatabaseDAO<T extends DatabaseModel> implements DAO<T> {

  /**
   * The factory to create entity managers for DB operations.
   */
  protected EntityManagerFactory entityManagerFactory;

  /**
   * The entity manager.
   */
  protected EntityManager entityManager;

  /**
   * The entity class for the main models.
   */
  protected final Class<? extends T> persistableClass;

  /**
   * Constructor.
   * 
   * @param persistableClass the class of the entity for reflective purposes
   * @param persistenceUnit  the persistence unit to load
   */
  protected AbstractDatabaseDAO(Class<? extends T> persistableClass, String persistenceUnit) {
    super();

    AssertUtils.notNull(persistableClass);

    this.persistableClass = persistableClass;

    ServiceRegistry.getLogProxy().info(this, "PersistenceUnit: {0}", persistenceUnit);

    entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnit);
    entityManager = entityManagerFactory.createEntityManager();
  }

  private EntityManager prepareManager() {
    // EntityManager entityManager = entityManagerFactory.createEntityManager();

    return entityManager;
  }

  private void prepareAuditFieldsSubPersistables(T persistable) {
    Field[] fields = persistable.getClass().getDeclaredFields();

    for (Field field : fields) {
      boolean annotationPresent = false;
      if (field.isAnnotationPresent(OneToOne.class)) {
        List<CascadeType> cascade = Arrays.asList(field.getAnnotation(OneToOne.class).cascade());
        annotationPresent |= cascade.contains(CascadeType.MERGE);
        annotationPresent |= cascade.contains(CascadeType.ALL);
      }
      if (field.isAnnotationPresent(OneToMany.class)) {
        List<CascadeType> cascade = Arrays.asList(field.getAnnotation(OneToMany.class).cascade());
        annotationPresent |= cascade.contains(CascadeType.MERGE);
        annotationPresent |= cascade.contains(CascadeType.ALL);
      }
      if (field.isAnnotationPresent(ManyToOne.class)) {
        List<CascadeType> cascade = Arrays.asList(field.getAnnotation(ManyToOne.class).cascade());
        annotationPresent |= cascade.contains(CascadeType.MERGE);
        annotationPresent |= cascade.contains(CascadeType.ALL);
      }
      if (field.isAnnotationPresent(ManyToMany.class)) {
        List<CascadeType> cascade = Arrays.asList(field.getAnnotation(ManyToMany.class).cascade());
        annotationPresent |= cascade.contains(CascadeType.MERGE);
        annotationPresent |= cascade.contains(CascadeType.ALL);
      }
      if (annotationPresent && AbstractDatabasePersistable.class.isAssignableFrom(field.getType())) {
        field.setAccessible(true);
        AbstractDatabasePersistable value;
        try {
          value = (AbstractDatabasePersistable) field.get(persistable);
        } catch (IllegalArgumentException | IllegalAccessException e) {
          ServiceRegistry.getLogProxy().error(this, "Could not adapt referenced model", e);
          continue;
        }
        if (value == null) {
          continue;
        }

        if (value.getRecordId() == null) {
          value.setCreator(persistable.getModifier());
          value.setDeletedFlag(false);
        }
        value.setModifier(persistable.getModifier());
        value.setAuditComment(persistable.getAuditComment());
      }
    }
  }

  /**
   * Checks whether the passed model is valid for performing DB operations.
   * 
   * @param persistable the model to check
   * @return Whether the model is valid.
   */
  protected boolean isValid(T persistable) {
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public T save(T inPersistable, Integer modifier) throws SaveException {
    AssertUtils.notNull(modifier, "Modifier must not be null");

    if (!isValid(inPersistable)) { throw new SaveException("Persistable is not valid"); }

    EntityManager entityManager = prepareManager();

    try {
      inPersistable.setModifier(modifier);
      prepareAuditFieldsSubPersistables(inPersistable);
      if (inPersistable.getRecordId() == null) {
        inPersistable.setCreator(modifier);
        entityManager.getTransaction().begin();
        entityManager.persist(inPersistable);
        entityManager.getTransaction().commit();
      } else {
        entityManager.getTransaction().begin();
        inPersistable = entityManager.merge(inPersistable);
        entityManager.getTransaction().commit();
      }

      return inPersistable;

    } catch (OptimisticLockException e) {
      throw new SaveException(e);
    } finally {
      if (entityManager != null) {
        if (entityManager.getTransaction().isActive()) {
          entityManager.getTransaction().rollback();
        }
        // entityManager.close()
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void delete(Integer recordId) throws DeleteException {
    if (!isDeletable(recordId)) { throw new DeleteException(); }

    EntityManager entityManager = prepareManager();

    try {

      entityManager.getTransaction().begin();
      T entity = entityManager.find(persistableClass, recordId);
      entityManager.refresh(entity);
      entity.setDeletedFlag(true);
      entityManager.getTransaction().commit();
    } catch (Exception e) {
      throw new DeleteException(e);
    } finally {
      if (entityManager != null) {
        if (entityManager.getTransaction().isActive()) {
          entityManager.getTransaction().rollback();
        }
        // entityManager.close()
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void remove(Integer recordId) throws DeleteException {
    if (!isDeletable(recordId)) { throw new DeleteException("Not deletable"); }

    EntityManager entityManager = prepareManager();

    try {

      entityManager.getTransaction().begin();
      T entity = entityManager.find(persistableClass, recordId);
      entityManager.remove(entity);
      entityManager.getTransaction().commit();
    } catch (Exception e) {
      throw new DeleteException(e);
    } finally {
      if (entityManager != null) {
        if (entityManager.getTransaction().isActive()) {
          entityManager.getTransaction().rollback();
        }
        // entityManager.close()
      }
    }
  }

  /**
   * Checks whether the model can be deleted with the current DB state.
   * 
   * @param recordId the ID of the model to check
   * @return whether delete operation is permitted.
   */
  public boolean isDeletable(Integer recordId) {
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public T load(Integer inIdentifier) {
    if (inIdentifier == null) { return null; }

    EntityManager entityManager = prepareManager();

    try {
      return entityManager.find(persistableClass, inIdentifier);
    } finally {
      // entityManager.close()
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Integer> loadEnversRevisionIds(Integer recordId) {
    EntityManager entityManager = prepareManager();

    try {
      AuditReader auditReader = AuditReaderFactory.get(entityManager);
      List<Number> revisions = auditReader.getRevisions(persistableClass, recordId);
      List<Integer> revisionIds = revisions.stream().map(n -> n.intValue()).collect(Collectors.toList());
      return revisionIds;
    } finally {
      // entityManager.close()
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public T loadHistory(Integer recordId, Integer revisionId) {
    List<Integer> revisions = loadEnversRevisionIds(recordId);

    EntityManager entityManager = prepareManager();

    try {
      AuditReader auditReader = AuditReaderFactory.get(entityManager);
      return auditReader.find(persistableClass, recordId, revisions.get(revisionId));

    } finally {
      // entityManager.close()
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public T loadHistoryEnvers(Integer recordId, Integer revisionId) {
    EntityManager entityManager = prepareManager();

    try {
      AuditReader auditReader = AuditReaderFactory.get(entityManager);
      return auditReader.find(persistableClass, recordId, revisionId);

    } finally {
      // entityManager.close()
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Integer> getAllIds() {
    return getAllIds(null, 0, Integer.MAX_VALUE);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Integer> getAllIds(int startIndex, int count) {
    return getAllIds(null, startIndex, count);
  }

  @Override
  public List<Integer> getAllIds(AbstractRestriction restriction, int startIndex, int count) {
    return getAllIds(restriction, startIndex, count, null);
  }

  @Override
  public List<Integer> getAllIds(AbstractRestriction restriction, int startIndex, int count,
      List<Pair<String, Boolean>> orders) {
    EntityManager entityManager = prepareManager();

    try {
      CriteriaBuilder builder = entityManager.getCriteriaBuilder();

      CriteriaQuery<Integer> query = builder.createQuery(Integer.class);

      Root<? extends T> root = query.from(persistableClass);
      query.select(root.get("recordId"));

      List<Order> hibernateOrders = createOrders(root, orders);
      if (hibernateOrders != null) {
        query.orderBy(hibernateOrders);
      }

      restriction = addNotDeletedRestriction(restriction);
      addRestriction(query, root, builder, restriction);

      List<Integer> ids = entityManager.createQuery(query).setFirstResult(startIndex).setMaxResults(count)
          .getResultList();
      return ids;
    } finally {
      // entityManager.close()
    }
  }

  @Override
  public List<T> find(AbstractRestriction restriction, int startIndex, int count) {
    return find(restriction, startIndex, count, null);
  }

  @Override
  public List<T> find(AbstractRestriction restriction, int startIndex, int count, List<Pair<String, Boolean>> orders) {
    EntityManager entityManager = prepareManager();

    try {
      CriteriaBuilder builder = entityManager.getCriteriaBuilder();

      CriteriaQuery<? extends T> query = builder.createQuery(persistableClass);

      Root<? extends T> root = query.from(persistableClass);

      restriction = addNotDeletedRestriction(restriction);
      addRestriction(query, root, builder, restriction);

      List<Order> hibernateOrders = createOrders(root, orders);
      if (hibernateOrders != null) {
        query.orderBy(hibernateOrders);
      }

      List<? extends T> found = entityManager.createQuery(query).setFirstResult(startIndex).setMaxResults(count)
          .getResultList();
      List<T> result = new LinkedList<>(found);
      return result;
    } finally {
      // entityManager.close()
    }
  }

  private List<Order> createOrders(From<?, ?> entity, List<Pair<String, Boolean>> orders) {
    if (orders == null) { return null; }

    List<Order> hibernateOrders = new LinkedList<>();

    for (Pair<String, Boolean> pair : orders) {
      String[] split = pair.getA().split("\\.");

      Expression<?> expr;
      if (split.length > 1) {
        Path<?> from = buildFrom(entity, pair.getA());
        expr = from.get(split[split.length - 1]);
      } else {
        expr = entity.get(pair.getA());
      }

      Order order = new OrderImpl(expr, pair.getB());
      hibernateOrders.add(order);
    }

    return hibernateOrders;
  }

  @Override
  public Long count() {
    return count(null);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Long count(AbstractRestriction restriction) {
    EntityManager entityManager = prepareManager();

    try {
      CriteriaBuilder builder = entityManager.getCriteriaBuilder();

      CriteriaQuery<Long> query = builder.createQuery(Long.class);

      Root<? extends T> root = query.from(persistableClass);
      query.select(builder.count(root));

      restriction = addNotDeletedRestriction(restriction);
      addRestriction(query, root, builder, restriction);

      return entityManager.createQuery(query).getSingleResult();
    } finally {
      // entityManager.close()
    }
  }

  @Override
  public boolean containsId(Integer identifier) {
    return containsId(null, identifier);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean containsId(AbstractRestriction restriction, Object identifier) {
    EntityManager entityManager = prepareManager();

    try {
      CriteriaBuilder builder = entityManager.getCriteriaBuilder();

      CriteriaQuery<Long> query = builder.createQuery(Long.class);

      Root<? extends T> root = query.from(persistableClass);
      query.select(builder.count(root));

      EqualityRestriction recordId = new EqualityRestriction("recordId", identifier);

      restriction = and(recordId, restriction);

      addRestriction(query, root, builder, restriction);

      return entityManager.createQuery(query).getSingleResult() > 0;
    } finally {
      // entityManager.close()
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object getProperty(Integer recordId, String propertyId) {
    EntityManager entityManager = prepareManager();

    try {
      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      CriteriaQuery<?> query = builder.createQuery();

      Root<? extends T> root = query.from(persistableClass);

      query.select(root.get(propertyId));

      query.where(builder.equal(root.get("recordId"), recordId));

      Object result = entityManager.createQuery(query).getSingleResult();
      return result;
    } finally {
      // entityManager.close()
    }
  }

  @Override
  public Long getPersistableIndex(Integer recordId) {
    return getPersistableIndex(null, recordId);
  }

  @Override
  public Long getPersistableIndex(AbstractRestriction restriction, Integer recordId) {
    return getPersistableIndex(restriction, recordId, null);
  }

  @Override
  public Long getPersistableIndex(AbstractRestriction restriction, Integer recordId,
      List<Pair<String, Boolean>> orders) {
    EntityManager entityManager = prepareManager();

    try {
      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);

      Root<? extends T> root = countQuery.from(persistableClass);

      countQuery.select(builder.count(root.get("recordId")));

      List<Order> hibernateOrders = createOrders(root, orders);
      if (hibernateOrders != null) {
        countQuery.orderBy(hibernateOrders);
      }

      LessEqualRestriction recordIdRestriction = new LessEqualRestriction("recordId", recordId);

      restriction = addNotDeletedRestriction(restriction);
      restriction = and(recordIdRestriction, restriction);
      addRestriction(countQuery, root, builder, restriction);

      // countQuery.where(builder.le(root.get("recordId"), recordId));
      countQuery.orderBy(builder.asc(root.get("recordId")));

      return entityManager.createQuery(countQuery).getSingleResult();
    } finally {
      // entityManager.close()
    }
  }

  @Override
  public Integer getFirstId() {
    return getFirstId(null);
  }

  @Override
  public Integer getFirstId(AbstractRestriction restriction) {
    return getFirstId(restriction, null);
  }

  @Override
  public Integer getFirstId(AbstractRestriction restriction, List<Pair<String, Boolean>> orders) {
    EntityManager entityManager = prepareManager();

    try {
      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      CriteriaQuery<Integer> query = builder.createQuery(Integer.class);

      Root<? extends T> root = query.from(persistableClass);

      List<Order> hibernateOrders = createOrders(root, orders);
      if (hibernateOrders != null) {
        query.orderBy(hibernateOrders);
      }

      addRestriction(query, root, builder, restriction);

      query.select(root.get("recordId"));
      query.orderBy(builder.asc(root.get("recordId")));

      return entityManager.createQuery(query).setMaxResults(1).getSingleResult();
    } finally {
      // entityManager.close()
    }
  }

  @Override
  public Integer getLastId() {
    return getLastId(null);
  }

  @Override
  public Integer getLastId(AbstractRestriction restriction) {
    return getLastId(restriction, null);
  }

  @Override
  public Integer getLastId(AbstractRestriction restriction, List<Pair<String, Boolean>> orders) {
    EntityManager entityManager = prepareManager();

    try {
      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      CriteriaQuery<Integer> query = builder.createQuery(Integer.class);

      Root<? extends T> root = query.from(persistableClass);

      List<Order> hibernateOrders = createOrders(root, orders);
      if (hibernateOrders != null) {
        query.orderBy(hibernateOrders);
      }

      restriction = addNotDeletedRestriction(restriction);
      addRestriction(query, root, builder, restriction);

      query.select(root.get("recordId"));
      query.orderBy(builder.asc(root.get("recordId")));

      return entityManager.createQuery(query).setFirstResult(count().intValue()).setMaxResults(1).getSingleResult();
    } finally {
      // entityManager.close()
    }
  }

  @Override
  public Integer getNextId(Integer recordId) {
    return getNextId(null, recordId);
  }

  @Override
  public Integer getNextId(AbstractRestriction restriction, Integer recordId) {
    return getNextId(restriction, recordId, null);
  }

  @Override
  public Integer getNextId(AbstractRestriction restriction, Integer recordId, List<Pair<String, Boolean>> orders) {
    EntityManager entityManager = prepareManager();

    try {
      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      int productIndex = getPersistableIndex(restriction, recordId).intValue();

      CriteriaQuery<Integer> query = builder.createQuery(Integer.class);

      Root<? extends T> root = query.from(persistableClass);

      List<Order> hibernateOrders = createOrders(root, orders);
      if (hibernateOrders != null) {
        query.orderBy(hibernateOrders);
      }

      restriction = addNotDeletedRestriction(restriction);
      addRestriction(query, root, builder, restriction);

      query.select(root.get("recordId"));
      query.orderBy(builder.asc(root.get("recordId")));

      List<Integer> resultList = entityManager.createQuery(query).setFirstResult(productIndex + 1).setMaxResults(2)
          .getResultList();

      return resultList.get(1);
    } finally {
      // entityManager.close()
    }
  }

  private AbstractRestriction addNotDeletedRestriction(AbstractRestriction restriction) {
    EqualityRestriction restrictionToAdd = new EqualityRestriction("deletedFlag", 1, true);
    if (restriction == null) { return restrictionToAdd; }

    return new ConjunctionRestriction(restriction, restrictionToAdd);
  }

  @Override
  public Integer getPreviousId(Integer recordId) {
    return getPreviousId(null, recordId);
  }

  @Override
  public Integer getPreviousId(AbstractRestriction restriction, Integer recordId) {
    return getPreviousId(restriction, recordId, null);
  }

  @Override
  public Integer getPreviousId(AbstractRestriction restriction, Integer recordId, List<Pair<String, Boolean>> orders) {
    EntityManager entityManager = prepareManager();

    try {
      CriteriaBuilder builder = entityManager.getCriteriaBuilder();

      int productIndex = getPersistableIndex(restriction, recordId).intValue();

      CriteriaQuery<Integer> query = builder.createQuery(Integer.class);

      Root<? extends T> root = query.from(persistableClass);

      List<Order> hibernateOrders = createOrders(root, orders);
      if (hibernateOrders != null) {
        query.orderBy(hibernateOrders);
      }

      restriction = addNotDeletedRestriction(restriction);
      addRestriction(query, root, builder, restriction);

      query.select(root.get("recordId"));
      query.orderBy(builder.asc(root.get("recordId")));

      return entityManager.createQuery(query).setFirstResult(productIndex - 1).setMaxResults(1).getSingleResult();
    } finally {
      // entityManager.close()
    }
  }

  /**
   * Performs an AND operation for two restrictions.
   * 
   * @param left  the left hand side restriction
   * @param right the right hand side restriction
   * @return the combined restriction.
   */
  protected AbstractRestriction and(AbstractRestriction left, AbstractRestriction right) {
    if (left == null) {
      return right;
    } else if (right == null) { return left; }

    return new ConjunctionRestriction(left, right);
  }

  /**
   * Adds the passed restriction to the query.
   * 
   * @param query       the query to add to
   * @param from        the from where the restriction applies
   * @param builder     the builder of the criteria of the query
   * @param restriction the restriction to apply
   */
  protected void addRestriction(CriteriaQuery<?> query, From<?, ?> from, CriteriaBuilder builder,
      AbstractRestriction restriction) {
    if (query == null || restriction == null) { return; }

    @SuppressWarnings("unchecked")
    Expression<Boolean> where = mapRestriction(from, builder, query, restriction);

    if (where == null) { return; }

    query.where(where);
  }

  /**
   * Returns the factory for entity managers.
   * 
   * @return the factory.
   */
  protected EntityManagerFactory getEntityManagerFactory() {
    return entityManagerFactory;
  }

  /**
   * maps the passed restriction to hibernate predicates.
   * 
   * @param from        the from where the restriction applies
   * @param builder     the builder of the criteria of the query
   * @param query       the query to later execute
   * @param restriction the restriction to apply
   * @return the hibernate expression that can be used further.
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  protected Expression mapRestriction(From from, CriteriaBuilder builder, CriteriaQuery query,
      AbstractRestriction restriction) {

    // Junctions

    if (restriction instanceof ConjunctionRestriction) {
      ConjunctionRestriction and = (ConjunctionRestriction) restriction;

      Expression<Boolean> left = mapRestriction(from, builder, query, and.getLeft());
      Expression<Boolean> right = mapRestriction(from, builder, query, and.getRight());

      Predicate expression = builder.and(left, right);
      if (and.isNot()) expression = expression.not();
      return expression;
    } else if (restriction instanceof DisjunctionRestriction) {
      DisjunctionRestriction or = (DisjunctionRestriction) restriction;

      Expression<Boolean> left = mapRestriction(from, builder, query, or.getLeft());
      Expression<Boolean> right = mapRestriction(from, builder, query, or.getRight());

      Predicate expression = builder.or(left, right);
      if (or.isNot()) expression = expression.not();
      return expression;
    }

    // Binary Restrictions

    else if (restriction instanceof EqualityRestriction) {
      EqualityRestriction eq = (EqualityRestriction) restriction;

      if (eq.isNot()) { return builder.notEqual(buildFrom(from, eq.getField()), eq.getValue()); }
      return builder.equal(buildFrom(from, eq.getField()), eq.getValue());
    } else if (restriction instanceof LessEqualRestriction) {
      LessEqualRestriction le = (LessEqualRestriction) restriction;

      if (le.isNot()) { return builder.gt(buildFrom(from, le.getField()), (Number) le.getValue()); }
      return builder.le(buildFrom(from, le.getField()), (Number) le.getValue());

    } else if (restriction instanceof LessThanRestriction) {
      LessThanRestriction lt = (LessThanRestriction) restriction;

      if (lt.isNot()) { return builder.ge(buildFrom(from, lt.getField()), (Number) lt.getValue()); }
      return builder.lt(buildFrom(from, lt.getField()), (Number) lt.getValue());

    } else if (restriction instanceof GreaterEqualRestriction) {
      GreaterEqualRestriction ge = (GreaterEqualRestriction) restriction;

      if (ge.isNot()) { return builder.lt(buildFrom(from, ge.getField()), (Number) ge.getValue()); }
      return builder.ge(buildFrom(from, ge.getField()), (Number) ge.getValue());

    } else if (restriction instanceof GreaterThanRestriction) {
      GreaterThanRestriction gt = (GreaterThanRestriction) restriction;

      if (gt.isNot()) { return builder.le(buildFrom(from, gt.getField()), (Number) gt.getValue()); }
      return builder.gt(buildFrom(from, gt.getField()), (Number) gt.getValue());

    } else if (restriction instanceof InRestriction) {
      InRestriction in = (InRestriction) restriction;

      Object[] values = (Object[]) in.getValue();
      if (values == null || values.length == 0) { return builder.ge(from.get("recordId"), 1); }
      if (values.length == 1) {
        if (in.isNot()) { return builder.notEqual(buildFrom(from, in.getField()), values[0]); }
        return builder.equal(buildFrom(from, in.getField()), values[0]);
      }

      Predicate[] predicates = new Predicate[values.length];
      for (int i = 0; i < values.length; i++) {
        if (in.isNot()) {
          predicates[i] = builder.notEqual(buildFrom(from, in.getField()), values[i]);
        } else {
          predicates[i] = builder.equal(buildFrom(from, in.getField()), values[i]);
        }
      }

      if (in.isNot()) { return builder.and(predicates); }
      return builder.or(predicates);

    } else if (restriction instanceof LikeRestriction) {
      LikeRestriction like = (LikeRestriction) restriction;

      if (like.isNot()) { return builder.notLike(buildFrom(from, like.getField()), (String) like.getValue()); }
      return builder.like(buildFrom(from, like.getField()), (String) like.getValue());

    }

    // Unary Restrictions

    else if (restriction instanceof NullRestriction) {
      NullRestriction nil = (NullRestriction) restriction;

      if (nil.isNot()) { return builder.isNotNull(buildFrom(from, nil.getField())); }
      builder.isNull(buildFrom(from, nil.getField()));
    }

    return null;
  }

  /**
   * Follows the passed from until it finds the passed field.
   * 
   * @param from  the from to follow
   * @param field the field to find
   * @return the found path on the from to the passed field.
   */
  @SuppressWarnings("rawtypes")
  protected Path buildFrom(From from, String field) {

    String[] parts = field.split("\\.");
    if (parts.length < 1) { return from; }
    if (parts.length == 1) { return from.get(parts[0]); }
    From<?, ?> out = from;

    for (int i = 0; i < parts.length - 1; i++) {
      out = out.join(parts[i], JoinType.LEFT);
    }

    return out.get(parts[parts.length - 1]);
  }
}
