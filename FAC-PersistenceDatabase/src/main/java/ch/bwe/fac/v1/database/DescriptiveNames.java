package ch.bwe.fac.v1.database;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The fields containing the name to be read by a human.
 * 
 * @author benjamin
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DescriptiveNames {
  /**
   * @return the names
   */
  DescriptiveName[] value();
}
