package ch.bwe.fac.v1.database;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fac.v1.type.Persistable;

/**
 * Base type for all models in the application.
 *
 * @author Benjamin Weber
 */
public interface DatabaseModel extends Persistable {

  /**
   * Base type for the fields in a model. This is intended to be used as central
   * location for the relevant field names if reflective access is needed, as is
   * the case for some Beans.
   * 
   * @author benjamin
   */
  public static class PropertyId {

    /**
     * The ID of the model on the database. This must only be null for unsaved
     * models.
     */
    public static final String RECORD_ID = "recordId";

    /**
     * The version number of the model. Null for unsaved models, is incremented with
     * each save, starting at 0.
     */
    public static final String REVISION_ID = "revisionId";

    /**
     * The ID of the user who created the model. Null for unsaved models.<br>
     * Reserved IDs: <br>
     * <table>
     * <tr>
     * <th>ID</th>
     * <th>Meaning</th>
     * </tr>
     * <tr>
     * <td>-1</td>
     * <td>Application Developer /<br>
     * Update Process</td>
     * </tr>
     * <tr>
     * <td>0</td>
     * <td>System</td>
     * </tr>
     * </table>
     */
    public static final String CREATOR = "creator";

    /**
     * The timestamp of the creation of the model. Null for unsaved models.
     */
    public static final String CREATION_DATE_TIME = "creationDateTime";

    /**
     * The ID of the user who last modified the model. Null for unsaved models.<br>
     * Reserved IDs: <br>
     * <table>
     * <tr>
     * <th>ID</th>
     * <th>Meaning</th>
     * </tr>
     * <tr>
     * <td>-1</td>
     * <td>Application Developer /<br>
     * Update Process</td>
     * </tr>
     * <tr>
     * <td>0</td>
     * <td>System</td>
     * </tr>
     * </table>
     */
    public static final String MODIFIER = "modifier";

    /**
     * The timestamp of the last modification of the model. Null for unsaved models.
     */
    public static final String MODIFICATION_DATE_TIME = "modificationDateTime";

    /**
     * Whether to treat the model as deleted. Cannot be null.
     */
    public static final String DELETED_FLAG = "deletedFlag";

    /**
     * The comment about the last modification. Can be null depending on
     * configuration.
     */
    public static final String AUDIT_COMMENT = "auditComment";

    /**
     * The types of the fields on this model.
     */
    public static final Map<String, Class<?>> PROPERTY_TYPES;
    static {
      Map<String, Class<?>> propertyTypes = new HashMap<>();

      propertyTypes.put(RECORD_ID, Integer.class);
      propertyTypes.put(REVISION_ID, Integer.class);
      propertyTypes.put(CREATOR, Integer.class);
      propertyTypes.put(CREATION_DATE_TIME, LocalDateTime.class);
      propertyTypes.put(MODIFIER, Integer.class);
      propertyTypes.put(MODIFICATION_DATE_TIME, LocalDateTime.class);
      propertyTypes.put(DELETED_FLAG, Boolean.class);
      propertyTypes.put(AUDIT_COMMENT, String.class);

      PROPERTY_TYPES = propertyTypes;
    }
  }

  /**
   * Gets the value of the specified field on the model.
   * 
   * @param field the field to get the value from
   * @return the value of the specified field.
   */
  default Object get(String field) {
    try {
      Method method = getClass().getMethod("get" + field.substring(0, 1).toUpperCase() + field.substring(1));
      return method.invoke(this);
    } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not find getter for field {0}", e, field);
      throw new RuntimeException(e);
    }
  }

  /**
   * Sets the value of the specified field.
   * 
   * @param field the field to set the value of
   * @param value the value to set
   */
  default void set(String field, Object value) {
    try {
      Class<?> valueType = value.getClass();
      if (Persistable.class.isAssignableFrom(valueType)) {
        valueType = getModelFromPersistable((Class<Persistable>) valueType);
      }
      if (Arrays.asList(valueType.getInterfaces()).contains(Set.class)) {
        valueType = Set.class;
      } else if (Arrays.asList(valueType.getInterfaces()).contains(List.class)) {
        valueType = List.class;
      }
      Method method = getClass().getMethod("set" + field.substring(0, 1).toUpperCase() + field.substring(1), valueType);
      method.invoke(this, value);
    } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not find getter for field {0}", e, field);
      throw new RuntimeException(e);
    }
  }

  default Class<? extends DatabaseModel> getModelFromPersistable(Class<Persistable> persistable) {
    Class<?>[] interfaces = persistable.getInterfaces();
    for (Class<?> ifc : interfaces) {
      for (Class<?> sup : ifc.getInterfaces()) {
        if (DatabaseModel.class.equals(sup)) { return (Class<? extends DatabaseModel>) ifc; }
      }
    }
    throw new IllegalArgumentException("Passed Persistable is not a Model: " + persistable.getName());
  }

  /**
   * Sets the technical ID on the DB.
   * 
   * @param recordId the value
   */
  void setRecordId(Integer recordId);

  /**
   * Gets the technical ID on the DB.
   * 
   * @return the value. Null if it has never been saved.
   */
  Integer getRecordId();

  /**
   * Sets the version number of the model.
   * 
   * @param revisionId the number
   */
  void setRevisionId(Integer revisionId);

  /**
   * Gets the version number of the model.
   * 
   * @return the version. Null if never saved, starts at 0.
   */
  Integer getRevisionId();

  /**
   * Sets the ID of the user who created the model.
   * 
   * @param creator the user ID
   */
  void setCreator(Integer creator);

  /**
   * Gets the ID of the user who created the model.
   * 
   * @return the ID. 0 for system, null if never saved.
   */
  Integer getCreator();

  /**
   * Sets the time the model has first been saved.
   * 
   * @param creationDateTime the creation time
   */
  void setCreationDateTime(LocalDateTime creationDateTime);

  /**
   * Returns the time the model has first been saved.
   * 
   * @return the time. Null if never saved.
   */
  LocalDateTime getCreationDateTime();

  /**
   * Sets the ID of the user who last modified the model.
   * 
   * @param modifier the user ID
   */
  void setModifier(Integer modifier);

  /**
   * Returns the ID of the user who last saved the model.
   * 
   * @return the user ID. 0 for system, null if never saved.
   */
  Integer getModifier();

  /**
   * Sets the time the model was last saved.
   * 
   * @param modificationDateTime the modification time
   */
  void setModificationDateTime(LocalDateTime modificationDateTime);

  /**
   * Returns the time the model was last saved.
   * 
   * @return the time. Null if never saved.
   */
  LocalDateTime getModificationDateTime();

  /**
   * Set whether this model is to be treated as deleted.
   * 
   * @param deletedFlag the flag
   */
  void setDeletedFlag(boolean deletedFlag);

  /**
   * Returns whether this model is to be treated as deleted.
   * 
   * @return the flag, cannot be null.
   */
  boolean getDeletedFlag();

  /**
   * Sets the comment for the last modification.
   * 
   * @param auditComment the modification comment
   */
  void setAuditComment(String auditComment);

  /**
   * Returns the comment for the last modification.
   * 
   * @return the comment. May be null, depends on configuration.
   */
  String getAuditComment();
}
