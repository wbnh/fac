package ch.bwe.fac.v1.database;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import ch.bwe.fac.v1.type.restriction.AbstractRestriction;
import ch.bwe.fac.v1.type.restriction.EqualityRestriction;
import ch.bwe.fac.v1.type.restriction.LessEqualRestriction;

/**
 * @author benjamin
 * @param <T> the type of class in which the model is embedded
 * @param <U> the type of class to fetch
 */
public class AbstractReferencedDatabaseDAO<T extends DatabaseModel, U extends DatabaseModel>
    extends AbstractDatabaseDAO<U> {

  /**
   * The class in which the objects are referenced.
   */
  protected final Class<? extends T> rootClass;

  /**
   * The attribute referencing the model we are interested in.
   */
  protected final String referenceAttribute;

  /**
   * Constructor.
   * 
   * @param rootClass          the class in which the objects are referenced
   * @param referenceAttribute the attribute referencing the model we are
   *                           interested in
   * @param persistableClass   the class to fetch
   * @param persistenceUnit    the unit to load
   */
  protected AbstractReferencedDatabaseDAO(Class<? extends T> rootClass, String referenceAttribute,
      Class<? extends U> persistableClass, String persistenceUnit) {
    super(persistableClass, persistenceUnit);

    this.rootClass = rootClass;
    this.referenceAttribute = referenceAttribute;
  }

  @Override
  public List<Integer> getAllIds(AbstractRestriction restriction, int startIndex, int count) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();

    try {

      CriteriaBuilder builder = entityManager.getCriteriaBuilder();

      CriteriaQuery<Integer> query = builder.createQuery(Integer.class);

      System.out.println("blah");
      System.out.println(referenceAttribute);
      System.out.println(restriction);

      Root<? extends T> root = query.from(rootClass);
      Join<T, U> join = root.join(referenceAttribute);

      query.select(join.get("recordId"));

      addRestriction(query, root, builder, restriction);

      List<Integer> ids = entityManager.createQuery(query).setFirstResult(startIndex).setMaxResults(count)
          .getResultList();
      return ids;

    } finally {
      if (entityManager != null) {
        entityManager.close();
      }
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<U> find(AbstractRestriction restriction, int startIndex, int count) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();

    try {

      CriteriaBuilder builder = entityManager.getCriteriaBuilder();

      CriteriaQuery<? extends U> query = builder.createQuery(persistableClass);

      Root<? extends T> root = query.from(rootClass);
      @SuppressWarnings("rawtypes")
      Join join = root.join(referenceAttribute);

      query.select(join);

      addRestriction(query, root, builder, restriction);

      List<? extends U> found = entityManager.createQuery(query).setFirstResult(startIndex).setMaxResults(count)
          .getResultList();
      List<U> result = new LinkedList<>(found);
      return result;

    } finally {
      if (entityManager != null) {
        entityManager.close();
      }
    }
  }

  @Override
  public Long count(AbstractRestriction restriction) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();

    try {

      CriteriaBuilder builder = entityManager.getCriteriaBuilder();

      CriteriaQuery<Long> query = builder.createQuery(Long.class);

      Root<? extends T> root = query.from(rootClass);
      Join<T, U> join = root.join(referenceAttribute);

      query.select(builder.count(join));

      addRestriction(query, root, builder, restriction);

      return entityManager.createQuery(query).getSingleResult();

    } finally {
      if (entityManager != null) {
        entityManager.close();
      }
    }
  }

  @Override
  public boolean containsId(AbstractRestriction restriction, Object identifier) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();

    try {

      CriteriaBuilder builder = entityManager.getCriteriaBuilder();

      CriteriaQuery<Long> query = builder.createQuery(Long.class);

      Root<? extends T> root = query.from(rootClass);
      Join<T, U> join = root.join(referenceAttribute);

      query.select(builder.count(join));

      EqualityRestriction recordId = new EqualityRestriction(referenceAttribute + ".recordId", identifier);

      restriction = and(recordId, restriction);

      addRestriction(query, root, builder, restriction);

      return entityManager.createQuery(query).getSingleResult() > 0;

    } finally {
      if (entityManager != null) {
        entityManager.close();
      }
    }
  }

  @Override
  public Long getPersistableIndex(AbstractRestriction restriction, Integer recordId) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    try {
      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);

      Root<? extends T> root = countQuery.from(rootClass);
      Join<T, U> join = root.join(referenceAttribute);

      countQuery.select(builder.count(join.get("recordId")));

      LessEqualRestriction recordIdRestriction = new LessEqualRestriction(referenceAttribute + ".recordId", recordId);

      restriction = and(recordIdRestriction, restriction);
      addRestriction(countQuery, root, builder, restriction);

      countQuery.orderBy(builder.asc(join.get("recordId")));

      return entityManager.createQuery(countQuery).getSingleResult();
    } finally {
      entityManager.close();
    }
  }

  @Override
  public Integer getFirstId(AbstractRestriction restriction) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    try {

      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      CriteriaQuery<Integer> query = builder.createQuery(Integer.class);

      Root<? extends T> root = query.from(rootClass);
      Join<T, U> join = root.join(referenceAttribute);

      addRestriction(query, root, builder, restriction);

      query.select(join.get("recordId"));
      query.orderBy(builder.asc(join.get("recordId")));

      return entityManager.createQuery(query).setMaxResults(1).getSingleResult();

    } finally {
      entityManager.close();
    }
  }

  @Override
  public Integer getLastId(AbstractRestriction restriction) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    try {

      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      CriteriaQuery<Integer> query = builder.createQuery(Integer.class);

      Root<? extends T> root = query.from(rootClass);
      Join<T, U> join = root.join(referenceAttribute);

      addRestriction(query, root, builder, restriction);

      query.select(join.get("recordId"));
      query.orderBy(builder.asc(join.get("recordId")));

      return entityManager.createQuery(query).setFirstResult(count().intValue()).setMaxResults(1).getSingleResult();

    } finally {
      entityManager.close();
    }
  }

  @Override
  public Integer getNextId(AbstractRestriction restriction, Integer recordId) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    try {

      CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      int productIndex = getPersistableIndex(restriction, recordId).intValue();

      CriteriaQuery<Integer> query = builder.createQuery(Integer.class);

      Root<? extends T> root = query.from(rootClass);
      Join<T, U> join = root.join(referenceAttribute);

      addRestriction(query, root, builder, restriction);

      query.select(join.get("recordId"));
      query.orderBy(builder.asc(join.get("recordId")));

      List<Integer> resultList = entityManager.createQuery(query).setFirstResult(productIndex + 1).setMaxResults(2)
          .getResultList();

      return resultList.get(1);

    } finally {
      entityManager.close();
    }
  }

  @Override
  public Integer getPreviousId(AbstractRestriction restriction, Integer recordId) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    try {

      CriteriaBuilder builder = entityManager.getCriteriaBuilder();

      int productIndex = getPersistableIndex(restriction, recordId).intValue();

      CriteriaQuery<Integer> query = builder.createQuery(Integer.class);

      Root<? extends T> root = query.from(rootClass);
      Join<T, U> join = root.join(referenceAttribute);

      addRestriction(query, root, builder, restriction);

      query.select(join.get("recordId"));
      query.orderBy(builder.asc(join.get("recordId")));

      return entityManager.createQuery(query).setFirstResult(productIndex - 1).setMaxResults(1).getSingleResult();

    } finally {
      entityManager.close();
    }
  }
}
