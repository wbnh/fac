package ch.bwe.fac.v1.database;

import java.time.LocalDateTime;

import ch.bwe.fac.v1.type.Persistable;

/**
 * @author benjamin
 */
public abstract class AbstractDatabasePersistable implements Persistable {

  /**
   * SUID.
   */
  private static final long serialVersionUID = 2313873022839842105L;

  /**
   * Constructor.
   */
  protected AbstractDatabasePersistable() {
    super();
    setDeletedFlag(getDeletedFlag());
  }

  /**
   * Converts between boolean and integer representation.
   * 
   * @param value the boolean value
   * @return the integer representation.
   */
  protected final Integer booleanToInteger(boolean value) {
    return value ? 1 : 0;
  }

  /**
   * Converts between boolean and integer representation.
   * 
   * @param value the integer representation
   * @return the boolean value.
   */
  protected final boolean integerToBoolean(Integer value) {
    if (value == null) { return false; }
    return value == 0 ? false : true;
  }

  /**
   * Returns the ID of the model.
   * 
   * @return the ID on the database. Null if unsaved.
   */
  public abstract Integer getRecordId();

  /**
   * Sets the ID of the model.
   * 
   * @param recordId the ID
   */
  public abstract void setRecordId(Integer recordId);

  /**
   * Returns the version number of the model.
   * 
   * @return the version. Null if unsaved. Starts at 0.
   */
  public abstract Integer getRevisionId();

  /**
   * Sets the version number of the model.
   * 
   * @param revisionId the version number
   */
  public abstract void setRevisionId(Integer revisionId);

  /**
   * Returns the ID of the person who created the model.
   * 
   * @return the creator. Null if unsaved, special values apply.
   */
  public abstract Integer getCreator();

  /**
   * Sets the ID of the person who created the model.
   * 
   * @param creator the creator
   */
  public abstract void setCreator(Integer creator);

  /**
   * Returns the time the model was first saved on the DB.
   * 
   * @return the original save time. Null if unsaved.
   */
  public abstract LocalDateTime getCreationDateTime();

  /**
   * Sets the time the model was first saved on the DB.
   * 
   * @param creationDateTime the time
   */
  public abstract void setCreationDateTime(LocalDateTime creationDateTime);

  /**
   * Returns the ID of the person who last saved the model.
   * 
   * @return the ID. Null if unsaved. Special values apply.
   */
  public abstract Integer getModifier();

  /**
   * Sets the ID of the person who last saved the model.
   * 
   * @param modifier the ID
   */
  public abstract void setModifier(Integer modifier);

  /**
   * Returns the time the model was last saved.
   * 
   * @return the time. Null if unsaved.
   */
  public abstract LocalDateTime getModificationDateTime();

  /**
   * Sets the time the model was last saved.
   * 
   * @param modificationDateTime the time
   */
  public abstract void setModificationDateTime(LocalDateTime modificationDateTime);

  /**
   * Returns whether this model shall be treated as deleted.
   * 
   * @return the flag.
   */
  public abstract boolean getDeletedFlag();

  /**
   * Sets whether this model shall be treated as deleted.
   * 
   * @param deletedFlag the flag
   */
  public abstract void setDeletedFlag(boolean deletedFlag);

  /**
   * Returns the comment for the last modification.
   * 
   * @return the comment. Can be null depending on configuration.
   */
  public abstract String getAuditComment();

  /**
   * Sets the comment for the last modification.
   * 
   * @param auditComment the comment
   */
  public abstract void setAuditComment(String auditComment);
}
