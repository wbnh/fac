package ch.bwe.fac.v1.database;

/**
 * DAO to provide functions using reflection and non-determined objects.
 *
 * @author Benjamin Weber
 */
public class ReflectiveDatabaseDAO extends AbstractDatabaseDAO<DatabaseModel> {

  /**
   * Constructor handling initialization of mandatory fields.
   *
   * @param persistableClassName the fully qualified name of the persistable.
   * @param persistenceUnit      the unit
   * @throws ClassNotFoundException if the passed class does not exist.
   */
  @SuppressWarnings("unchecked")
  public ReflectiveDatabaseDAO(String persistableClassName, String persistenceUnit) throws ClassNotFoundException {
    super((Class<DatabaseModel>) ReflectiveDatabaseDAO.class.getClassLoader().loadClass(persistableClassName),
        persistenceUnit);
  }

}
