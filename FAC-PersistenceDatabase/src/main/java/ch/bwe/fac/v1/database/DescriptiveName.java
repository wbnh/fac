package ch.bwe.fac.v1.database;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The field containing the name to be read by a human.
 * 
 * @author benjamin
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(DescriptiveNames.class)
public @interface DescriptiveName {
  /**
   * @return the name
   */
  String value();
}
